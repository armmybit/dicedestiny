﻿using UnityEngine;
using UnityEngine.Playables;

namespace Cinemachine.Examples
{
    [AddComponentMenu("")] // Don't display in add component menu
    public class ActivateTimeline : MonoBehaviour
    {
        private PlayableDirector director;

        // Use this for initialization
        private void Start()
        {
            director = GetComponent<PlayableDirector>();
        }

        private void OnTriggerEnter(Collider col)
        {
            if (col.CompareTag("Player"))
            {
                director.Play();
            }
        }

        private void OnTriggerExit(Collider col)
        {
            if (col.CompareTag("Player"))
            {
                director.Stop();
            }
        }
    }
}