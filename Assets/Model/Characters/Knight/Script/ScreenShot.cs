﻿using UnityEngine;

public class ScreenShot : MonoBehaviour
{
    private string format;

    // Use this for initialization
    private void Start()
    {
        format = "yyyy-MM-dd-HH-mm-ss";
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ScreenCapture.CaptureScreenshot(Application.dataPath + "/" + System.DateTime.Now.ToString(format) + ".png");
        }
    }
}