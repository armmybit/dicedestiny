﻿using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;

public static class SampleDB
{
    private const string TAG = "SampleDB";

    // Server Dat
    private static ServerPlayerModel serverPlayer;
    private static ServerMonsterModel serverModel;

    // Client Data
    private static PlayerModel player;

    public static void LoadServerPlayers()
    {
        LoadderLocal.Load("Data/players", (result) =>
        {
            serverPlayer = JsonConvert.DeserializeObject<ServerPlayerModel>(result);

            JulietLogger.Info(TAG, JsonConvert.SerializeObject(serverPlayer));
        });
    }

    public static void LoadClientPlayer()
    {
        LoadderLocal.Load("Data/player", (result) =>
        {
            player = JsonConvert.DeserializeObject<PlayerModel>(result);

            JulietLogger.Info(TAG, JsonConvert.SerializeObject(player));
        });
    }

    public static void LoadServerMonsters()
    {
        LoadderLocal.Load("Data/monsters", (result) =>
        {
            serverModel = JsonConvert.DeserializeObject<ServerMonsterModel>(result);

            JulietLogger.Info(TAG, JsonConvert.SerializeObject(serverModel));
        });
    }

    public static ServerPlayerProfileModel GetServerPlayer(int id)
    {
        return serverPlayer.Data.Find(x => x.Id == id);
    }

    public static PlayerModel GetPlayer()
    {
        return player;
    }

    public static ServerMonsterProfileModel GetMonster(int round, int leftPlayer)
    {
        serverModel.Data[0].Collections.Shuffle();

        if (round <= 3)
        {
            var model = serverModel.Data[0].Collections[0];

            return model;
        }
        else if (round <= 5)
        {
            var model = serverModel.Data[0].Collections[0];

            return model;
        }
        else if (leftPlayer == 2) // In case of game almost over/ player 2 left
        {
            var model = serverModel.Data[0].Collections[0];

            return model;
        }
        else
        {
            var model = serverModel.Data[0].Collections[0];

            return model;
        }
    }

    public static List<ServerPlayerProfileModel> GetBots(int amount)
    {
        List<ServerPlayerProfileModel> players = new List<ServerPlayerProfileModel>();
        int i = 1;

        for (; i <= amount; i++)
        {
            var join = new ServerPlayerJoinModel();
            int id = i + 1;
            var profile = GetServerPlayer(id);

            join.Basement = new PlayerFormBasementModel()
            {
                Id = 0
            };

            join.Main = new PlayerFormCharacterModel()
            {
                CharacterId = 0,
                ClassId = 0
            };

            join.Sub1 = new PlayerFormCharacterModel()
            {
                CharacterId = 0,
                ClassId = 1
            };

            join.Sub2 = new PlayerFormCharacterModel()
            {
                CharacterId = 1,
                ClassId = 0
            };


            profile.Join = join;
            profile.Status.IsBot = true;

            players.Add(profile);
        }

        return players;
    }
}