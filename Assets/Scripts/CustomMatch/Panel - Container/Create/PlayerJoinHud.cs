﻿using UnityEngine;

public class PlayerJoinHud : MonoBehaviour
{
    // GameObject
    public GameObject join;
    public GameObject empty;
    public GameObject containerMain;

    // UI
    public UnityEngine.UI.Text textName;
    public UnityEngine.UI.Text textLevel;

    public void Join(PlayerJoin player, GameObject model)
    {
        SetContent(player);
        SetCharacter(model);
        Toggle(true, false);
    }

    public void Leave()
    {
        Toggle(false, true);
    }

    private void SetContent(PlayerJoin player)
    {
        this.textName.text = player.Profile.Name;
        this.textLevel.text = "Lv. " + player.Profile.Level;
    }

    private void SetCharacter(GameObject model)
    {
        Util.AddChild(this.containerMain, model);
    }

    private void Toggle(bool isJoin, bool isEmpty)
    {
        this.join.SetActive(isJoin);
        this.empty.SetActive(isEmpty);
    }
}
