﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonStart : MonoBehaviour
{
    public void OnClick_Start()
    {
        SceneManager.LoadScene(1);
    }
}
