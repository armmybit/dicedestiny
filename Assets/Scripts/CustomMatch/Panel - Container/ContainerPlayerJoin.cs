﻿using System.Collections.Generic;
using UnityEngine;

public class ContainerPlayerJoin : MonoBehaviour
{
    // Refference
    public PlayerJoinHud[] playersHud;

    // Prepare
    public GameObject playerModel;

    public void Create(List<PlayerJoin> players)
    {
        int i = 0;

        for (; i < players.Count; i++)
        {
            var playerHud = playersHud[i];
            var player = players[i];

            playerHud.Join(player, playerModel);
        }
    }
}
