﻿using CI.TaskParallel;
using UnityEngine;

public class CustomMatchController : MonoBehaviour
{
    #region Singleton

    private static CustomMatchController _instance;

    public static CustomMatchController Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }

    #endregion

    public ContainerPlayerJoin containerJoin;
    public DebugMode mode;

    private void Start()
    {
        Application.runInBackground = true;
        JulietLogger.DebugMode = mode;

        // Init Unity Task to use theading
        UnityTask.InitialiseDispatcher();

        // Loading mock up data
        SampleDB.LoadClientPlayer();
        SampleDB.LoadServerPlayers();
        SampleDB.LoadServerMonsters();

        // Set mock up player Profile
        User.Set(SampleDB.GetPlayer());

        // Room Setting
        RoomSetting.MapId = 0;
        RoomSetting.MaxPLayer = 4;

        CreateRoom();
    }

    public void CreateRoom()
    {
        UnityTask.Run(() =>
        {
            var reqPlayer = new PlayerJoinModel()
            {
                Id = User.Profile.Id,
                Basement = User.CurrentIngame.Basement,
                Main = User.CurrentIngame.Main,
                Sub1 = User.CurrentIngame.Sub1,
                Sub2 = User.CurrentIngame.Sub2
            };

            var req = new CreateRoomRequest()
            {
                MapId = RoomSetting.MapId,
                MaxPlayer = RoomSetting.MaxPLayer,
                Player = reqPlayer
            };

            ServerHub.CreateRoom(req);
        });
    }

    public void OnCreateRoom(CreateRoomResponse res)
    {
        RoomSetting.RoomId = res.RoomId;
        RoomSetting.Players.AddRange(res.Players);

        UnityTask.RunOnUIThread(() =>
        {
            containerJoin.Create(res.Players);
        });
    }
}