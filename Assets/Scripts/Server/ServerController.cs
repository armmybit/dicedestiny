﻿using CI.TaskParallel;
using FullInspector;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerController : BaseBehavior<JsonNetSerializer>, IServer
{
    #region Singleton

    private static ServerController _instance;

    public static ServerController Instance
    {
        get
        {
            return _instance;
        }
    }

    protected override void Awake()
    {
        base.Awake();

        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
    }

    #endregion Singleton

    private const string TAG = "ServerController";

    public List<ServerRoom> rooms = new List<ServerRoom>();
    public List<ServerBuff> buffs = new List<ServerBuff>();
    public ServerMapModel map { get; set; }

    #region Internal Method

    private void Start()
    {
        OnceLoadGameBuffResources();
        OnceLoadMapResources();
    }

    private ServerRoom GetRoom(string roomId)
    {
        return rooms.Find(x => x.RoomId == roomId);
    }

    private string GeneratreRoom()
    {
        return "Room_" + (rooms.Count + 1);
    }

    #endregion Internal Method

    #region Resources

    private void OnceLoadGameBuffResources()
    {
        ServerBuffModel resources = null;

        LoadderLocal.Load("Data/shop", (result) =>
        {
            resources = JsonConvert.DeserializeObject<ServerBuffModel>(result);
        });

        ServerBuff shopInfo = new ServerBuff(BuffType.Shop, resources);

        LoadderLocal.Load("Data/event", (result) =>
        {
            resources = JsonConvert.DeserializeObject<ServerBuffModel>(result);
        });

        ServerBuff eventInfo = new ServerBuff(BuffType.Event, resources);

        LoadderLocal.Load("Data/card", (result) =>
        {
            resources = JsonConvert.DeserializeObject<ServerBuffModel>(result);
        });

        ServerBuff cardInfo = new ServerBuff(BuffType.Card, resources);

        this.buffs.Add(shopInfo);
        this.buffs.Add(eventInfo);
        this.buffs.Add(cardInfo);
    }

    private void OnceLoadMapResources()
    {
        LoadderLocal.Load("Data/map", (result) =>
        {
            map = JsonConvert.DeserializeObject<ServerMapModel>(result);
        });
    }

    #endregion Resources

    public void CreateRoom(string reqStr)
    {
        var req = JsonConvert.DeserializeObject<ServerCreateRoomRequest>(reqStr);
        var roomId = GeneratreRoom();

        ServerRoom room = new ServerRoom();
        ServerMapInfoModel mapInfo = map.Data.Find(x => x.Id == req.MapId);

        room.Setup(roomId, req, mapInfo, buffs);

        this.rooms.Add(room);

        OnCreateRoom(room);
    }

    public void OnCreateRoom(ServerRoom room)
    {
        ServerCreateRoomResponse res = new ServerCreateRoomResponse()
        {
            RoomId = room.RoomId,
            Players = room.GetPlayerJoin()
        };

        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnCreateRoom(resStr);
    }

    public void JoinRoom(string reqStr)
    {
        //var req = JsonConvert.DeserializeObject<ServerCreateRoomRequest>(reqStr);
    }

    public void OnJoinRoom(bool isJoined)
    {
        throw new System.NotImplementedException();
    }

    public void PlayerConnectedToGamePlay(string reqStr)
    {
        var req = JsonConvert.DeserializeObject<ServerPlayerConnectedRequest>(reqStr);
        var room = GetRoom(req.RoomId);
        bool isSuccess = room.Connected(req.PlayerId);

        if (room.CheckPlayerConnected() == room.MaxPlayer)
        {
            StartCoroutine(State(1f, GameState.ChooseOrder, room));
        }
        else
        {
            OnPlayerConnectedToGamePlay(isSuccess);
        }
    }

    public void OnPlayerConnectedToGamePlay(bool isSuccess)
    {
        var res = new ServerPlayerConnectedResponse()
        {
            IsSuccess = isSuccess
        };

        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnPlayerConnected(resStr);
    }

    public IEnumerator State(float deltaTime, GameState state, ServerRoom room = null)
    {
        room.ChangeGameState(GameState.ChooseOrder);

        yield return new WaitForSeconds(deltaTime);
        
        switch (state)
        {
            case GameState.ChooseOrder:
                CompletedPlayersConnected(state, room);
                break;

            case GameState.ShowOrder:
                OnPlayerChooseOrder(state, room);
                break;

            case GameState.Prepare:
                PrepareGame(room);
                break;

            case GameState.DrawCard:
                DrawCards(state, room);
                break;

            case GameState.Start:
                StartGame(state, room);
                break;

            case GameState.End:
                EndGame(room);
                break;
        }
    }

    public void CompletedPlayersConnected(GameState state, ServerRoom room)
    {
        //room.SetOrder();

        var res = new ServerConnectedPlayersResponse()
        {
            GameState = state
        };

        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnCompletedPlayersConnected(resStr);
    }

    public void PlayerChooseOrder(string reqStr)
    {
        var req = JsonConvert.DeserializeObject<ServerChooseOrderRequest>(reqStr);
        var room = GetRoom(req.RoomId);

        StartCoroutine(State(0f, GameState.ShowOrder, room));
        StartCoroutine(State(5f, GameState.Prepare, room));
    }

    public void OnPlayerChooseOrder(GameState state, ServerRoom room)
    {
        var res = new ServerChooseOrderResponse()
        {
            GameState = state,
            Numbers = room.GetPlayersOrder()
        };

        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnPlayerChooseOrder(resStr);
    }

    public void PrepareGame(ServerRoom room)
    {
        var state = room.ChangeGameState(GameState.Prepare);

        var blocks = room.blocks;
        var orders = room.GetPlayersOrder();

        var res = new ServerPrepareGameResponse()
        {
            GameState = state,
            Blocks = blocks,
            Orders = orders
        };

        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnPrepareGame(resStr);

        UnityTask.RunOnUIThread(() =>
        {
            StartCoroutine(State(1f, GameState.Start, room));
        });
    }

    public void StartGame(GameState state, ServerRoom room)
    {
        var id = room.GetTurn();
        var round = room.GetRound();

        var res = new ServerStartGameResponse()
        {
            GameState = state,
            Id = id,
            Round = round,
            IsCardAvailable = false
        };

        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.StartGame(resStr);
    }

    public void Roll(string reqStr)
    {
        var req = JsonConvert.DeserializeObject<ServerRollRequest>(reqStr);
        var room = GetRoom(req.RoomId);

        var face = room.RollDice(req.Roll);

        OnRoll(face);
    }

    public void OnRoll(ServerDiceFace diceFace)
    {
        var res = new ServerRollResponse()
        {
            DiceFace = diceFace
        };

        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnRoll(resStr);
    }

    public void PlayerArrived(string reqStr)
    {
        var req = JsonConvert.DeserializeObject<ServerPlayerArrivedRequest>(reqStr);

        switch (req.State)
        {
            case BlockCheckState.DetectOpponent:
                DetectOpponent(req);
                break;
            case BlockCheckState.DetectBlockType:
                DetectBlockType(req);
                break;
        }
    }

    public void DetectOpponent(ServerPlayerArrivedRequest req)
    {
        var room = GetRoom(req.RoomId);
        var opponent = room.DetectOpponent(req.Id, req.Info);

        if (opponent != null)
        {
            var player = room.GetPlayer(req.Id);
            OnDetectOpponent(opponent, player);
        }
        else
            DetectBlockType(req);
    }

    public void OnDetectOpponent(ServerAttackResponse opponent, ServerPlayer player)
    {
        var res = new ServerDetectOpponentResponse()
        {
            Opponent = opponent,
            BlockUpdate = player.Info.BreakPoints
        };

        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnDetectOpponent(resStr);
    }

    public void DetectBlockType(ServerPlayerArrivedRequest req)
    {
        var room = GetRoom(req.RoomId);
        var res = room.DetectBlockType(req.Id, req.Info);

        OnDetectBlockType(res);
    }

    public void OnDetectBlockType(ServerDetectBlockResponse res)
    {
        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnDetectBlockType(resStr);
    }

    public void Attack(string reqStr)
    {
        var req = JsonConvert.DeserializeObject<ServerAttackRequest>(reqStr);
        var room = GetRoom(req.RoomId);

        switch (req.AttackerType)
        {
            case CharacterType.Player:
                var res = room.PlayerAttack(req.AttackerId, req.DefenderId, req.DefenderType);
                OnPlayerAttack(res);

                break;
            case CharacterType.Monster:
                break;
        }
    }

    public void OnPlayerAttack(ServerAttackResponse res)
    {
        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnPlayerAttack(resStr);
    }

    public void BuyItem(string reqStr)
    {
        var req = JsonConvert.DeserializeObject<ServerBuyItemRequest>(reqStr);
        var room = GetRoom(req.RoomId);

        if (req.IsBuy)
        {
            var res = room.BuyItem(req.Id, req.ShopId);
            OnBuyItem(res);
        }
        else
            OnCancelBuyItem();
    }

    public void OnBuyItem(ServerBuyItemResponse res)
    {
        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnBuyItem(resStr);
    }

    public void OnCancelBuyItem()
    {
        var res = new ServerBuyItemResponse()
        {
            IsCancel = true
        };

        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnBuyItem(resStr);
    }

    public void ReceiveEvent(string reqStr)
    {
        var req = JsonConvert.DeserializeObject<ServerReceiveEventRequest>(reqStr);
        var room = GetRoom(req.RoomId);

        var res = room.ReceiveEvent(req.Id, req.EventId);
        OnReceiveEvent(res);
    }

    public void OnReceiveEvent(ServerReceiveEventResponse res)
    {
        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnReceiveEvent(resStr);
    }

    public void EndTurn(string reqStr)
    {
        var req = JsonConvert.DeserializeObject<ServerRollRequest>(reqStr);
        var room = GetRoom(req.RoomId);

        var alives = room.CountPlayerAlive();

        if (alives == 1)
        {
            StartCoroutine(State(1f, GameState.End, room));
            return;
        }

        var nextId = room.GetNextPlayerTurn(req.Id);
        var round = room.GetRound();
        var buffs = room.GetPlayerBuffs(nextId);
        var card = new ServerPlayerDrawCard();

        if (round > 1)
            card = room.PlayerDrawCard(DrawCard.One, nextId);

        NextTurn(nextId, round, buffs, card);
    }

    public List<ServerPlayerDrawCard> DrawCards(GameState state, ServerRoom room)
    {
        //room.PlayerDrawCard(DrawCard.One);

        //var cards = room.GetPlayerCard();

        return null;
    }

    public void NextTurn(int nextId, int round, Dictionary<BuffType, List<ServerPlayerBuffOnGoing>> buffs, ServerPlayerDrawCard card)
    {
        var res = new ServerEndTurnResponse()
        {
            Id = nextId,
            Round = round,
            Buffs = buffs,
            Card = card
        };

        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnNextTurn(resStr);
    }

    public void EndGame(ServerRoom room)
    {
        var winner = room.GetWinner();
        var loosers = room.GetLoosers();

        OnEndGame(winner, loosers);
    }

    public void OnEndGame(int winner, List<int> loosers)
    {
        var res = new ServerEndGameResponse()
        {
            Winner = winner,
            Loosers = loosers
        };

        var resStr = JsonConvert.SerializeObject(res);

        ServerHub.OnEndGame(resStr);
    }
}