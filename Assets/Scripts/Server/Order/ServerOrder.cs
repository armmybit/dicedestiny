﻿
using System;
using System.Collections.Generic;
using System.Linq;

public class ServerOrder
{
    public List<int> numbers = new List<int>();

    public void CreateOrder(int amount)
    {
        int order = 1;

        for (; order <= amount; order++)
            this.numbers.Add(order);

        this.numbers = this.numbers.OrderBy(o => Guid.NewGuid()).ToList();
    }

    public int ChooseNumber(int index)
    {
        int number = this.numbers[index];

        if (number > 0)
            this.numbers[index] = -1;

        return number;
    }
}
