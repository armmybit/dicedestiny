﻿using System;
using UnityEngine;

[Serializable]
public class ServerCard : ServerShopEffectState
{
    public override void Apply(ServerBuffInfoModel b, ref ServerRoom r, ref ServerPlayer s, ref ServerPlayer t)
    {
        base.Apply(b, ref r, ref s, ref t);
    }

    public override void Start()
    {
        base.Start();

        Set(buff.Id);
    }

    public override void Use()
    {
        base.Use();
    }

    public override void End()
    {
        base.End();

        Restore(buff.Id, buff.IsReset);
    }
}