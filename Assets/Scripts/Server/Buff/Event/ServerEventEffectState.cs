﻿using System;
using System.Collections.Generic;

[Serializable]
public class ServerEventEffectState : IServerBuff
{
    public ServerEventEffectStateModel Model { get; set; }

    protected void Set(int id)
    {
        Model = new ServerEventEffectStateModel();

        EventName name = (EventName)id;

        switch (name)
        {
            case EventName.AmorBreak:
                AmorBreak(false);
                break;
            case EventName.WeaponBreak:
                WeaponBreak(false);
                break;
            case EventName.Damage:
                Damage(false);
                break;
            case EventName.MonsterInvades:
                MonsterInvades(1, 4); // TODO
                break;
            case EventName.Heal:
                Heal();
                break;
            case EventName.DamageUp:
                DamageUp(false);
                break;
            case EventName.DefenseUp:
                DefenseUp(false);
                break;
            case EventName.MeteorStrike:
                MeteorStrike();
                break;
            case EventName.StormGust:
                StormGust(false);
                break;
            case EventName.ReStart_Teleport:
                Restart();
                break;
            case EventName.Reverse:
                Reverse();
                break;
            case EventName.Slow:
                Slow();
                break;
            case EventName.SpeedBoost:
                SpeedBoost();
                break;
        }
    }

    protected void Restore(int id, bool isReset)
    {
        EventName name = (EventName)id;

        switch (name)
        {
            case EventName.AmorBreak:

                if (isReset)
                    AmorBreak(true);

                break;
            case EventName.WeaponBreak:

                if (isReset)
                    WeaponBreak(true);

                break;
            case EventName.Damage:

                if (isReset)
                    Damage(true);

                break;

            case EventName.DamageUp:

                if (isReset)
                    Damage(true);

                break;
            case EventName.DefenseUp:

                if (isReset)
                    DefenseUp(true);

                break;
            case EventName.StormGust:

                if (isGotEffect)
                    StormGust(true);

                break;

            case EventName.MeteorStrike:
            case EventName.Heal:
            case EventName.ReStart_Teleport:
            case EventName.Reverse:
            case EventName.Slow:
            case EventName.SpeedBoost:
                break;
        }
    }

    private void AmorBreak(bool isTurnOver)
    {
        Model.Def = 1;

        if (isTurnOver)
            self.Info.SetDef(Model.Def);
        else
            self.Info.SetDef(-Model.Def);
    }

    private void WeaponBreak(bool isTurnOver)
    {
        Model.Atk = 1;

        if (isTurnOver)
            self.Info.SetAtk(Model.Atk);
        else
            self.Info.SetAtk(-Model.Atk);
    }

    private void Damage(bool isTurnOver)
    {
        Model.Hp = 1;

        if (isTurnOver)
            self.Info.SetHp(Model.Hp);
        else
            self.Info.SetHp(-Model.Hp);

    }

    private void MeteorStrike()
    {
        Model.Hp = 1;

        int blockImpact = UnityEngine.Random.Range(7, 12);
        var blocks = new List<ServerBlock>(room.blocks);
        var amount = (blocks.Count - blockImpact);

        blocks.Shuffle();
        blocks.RemoveRange(0, amount);

        blocks.ForEach(block =>
        {
            int playerId = -1;

            block.PlayersOnBlock.ForEach(id =>
            {
                var player = room.players.Find(x => x.Profile.Id == id && !x.Info.IsDied);
                player.Info.SetHp(-Model.Hp);

                playerId = player.Profile.Id;
            });

            Model.BlockIndexes.Add(block.Index, playerId);
        });
    }

    private void StormGust(bool isTurnOver)
    {
        int blockImpact = UnityEngine.Random.Range(7, 12);
        var blocks = new List<ServerBlock>(room.blocks);

        blocks.Shuffle();
        blocks.RemoveRange(blockImpact, blocks.Count);

        blocks.ForEach(block =>
        {
            block.PlayersOnBlock.ForEach(player =>
            {
                // TODO get Storm Gust to freeze enemy
            });
        });
    }

    private void MonsterInvades(int turn, int survival)
    {
        if (turn >= 7 || survival == 2)
        {
            // Get MVP Monster
            Model.IsLowBossBorn = true;
        }
        else if (turn <= 3)
        {
            // Get Low Tier Monster
            Model.IsMiddleBossBorn = true;
        }
        else if (turn <= 5)
        {
            // Get Middle Tier Monster
            Model.IsTopBossBorn = true;
        }
        else if (turn > 5)
        {
            // Get Top Tier Monster
            Model.IsMvpBossBorn = true;
        }
    }

    private void Heal()
    {
        Model.Hp = 2;

        self.Info.SetHp(Model.Hp);
    }

    private void DamageUp(bool isTurnOver)
    {
        Model.Atk = 1;

        if (isTurnOver)
            self.Info.SetAtk(Model.Atk);
        else
            self.Info.SetAtk(Model.Atk);

    }

    private void DefenseUp(bool isTurnOver)
    {
        Model.Def = 1;

        if (isTurnOver)
            self.Info.SetDef(-Model.Def);
        else
            self.Info.SetDef(Model.Def);

    }

    private void Restart()
    {
        Model.BlockIndex = room.blocks.FindIndex(x => x.OwnerBlock == self.Profile.Id);
    }

    private void Reverse()
    {
        room.IsGoAhead = !room.IsGoAhead;
    }

    private void Slow()
    {
        Model.DiceFace = new DiceFace()
        {
            Number1 = UnityEngine.Random.Range(1, 3),
            Number2 = UnityEngine.Random.Range(1, 3)
        };
    }

    private void SpeedBoost()
    {
        Model.DiceFace = new DiceFace()
        {
            Number1 = UnityEngine.Random.Range(3, 6),
            Number2 = UnityEngine.Random.Range(3, 6)
        };
    }
}

public class ServerEventEffectStateModel
{
    public double Hp { get; set; }
    public double Atk { get; set; }
    public double Def { get; set; }
    public int BlockIndex { get; set; }
    public Dictionary<int, int> BlockIndexes = new Dictionary<int, int>();
    public DiceFace DiceFace { get; set; }

    // Boss
    public bool IsLowBossBorn { get; set; }
    public bool IsMiddleBossBorn { get; set; }
    public bool IsTopBossBorn { get; set; }
    public bool IsMvpBossBorn { get; set; }
}