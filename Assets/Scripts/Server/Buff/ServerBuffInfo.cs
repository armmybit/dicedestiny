﻿
public struct ServerBuff
{
    public BuffType TypeBuff { get; set; }
    public ServerBuffModel Model { get; set; }

    public ServerBuff(BuffType typeBuff, ServerBuffModel model)
    {
        TypeBuff = typeBuff;
        Model = model;
    }
}