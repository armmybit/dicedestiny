﻿using System;

[Serializable]
public class ServerShopEffectState : IServerBuff
{
    public ServerShopEffectStateModel Model { get; set; }

    protected void Set(int id)
    {
        Model = new ServerShopEffectStateModel();
        ShopName name = (ShopName)id;

        switch (name)
        {
            case ShopName.TheDice:
                TheDice();
                break;
            case ShopName.RedPotion:
                RedPotion();
                break;
            case ShopName.Wing:
                Wing();
                break;
            case ShopName.TomeOfKnowledge:
                TomeOfKnowledge();
                break;
            case ShopName.GoddessTear:
                GoddessTear();
                break;
            case ShopName.AttackPotion:
                AttackPotion(false);
                break;
            case ShopName.DefPotion:
                DefensePotion(false);
                break;
        }
    }

    protected void Restore(int id, bool isReset)
    {
        ShopName name = (ShopName)id;

        switch (name)
        {
            case ShopName.TheDice:
            case ShopName.RedPotion:
            case ShopName.Wing:
            case ShopName.TomeOfKnowledge:
            case ShopName.GoddessTear:
                break;

            case ShopName.AttackPotion:
                if (isReset)
                    AttackPotion(true);
                break;
            case ShopName.DefPotion:
                if (isReset)
                    DefensePotion(true);
                break;
        }
    }

    private void TheDice()
    {
        Model.IsContinueRoll = true;
    }

    private void RedPotion()
    {
        Model.Hp = 3;

        self.Info.SetHp(Model.Hp);
    }

    private void Wing()
    {
        Model.BlockIndex = UnityEngine.Random.Range(0, 35);
    }

    private void TomeOfKnowledge()
    {
        Model.LevelUp = 1;
    }

    private void GoddessTear()
    {
        if (self.Info.IsDied)
        {
            Model.IsHeal = true;
            Model.Hp = 1;

            self.Info.SetHp(Model.Hp);
        }
        else if (self.Info.Status.Hp < 2)
        {
            Model.IsHeal = true;
            Model.Hp = 4;

            self.Info.SetHp(Model.Hp);
        }
    }

    private void AttackPotion(bool isTurnOver)
    {
        Model.Atk = 2;

        if (isTurnOver)
            self.Info.SetAtk(-Model.Atk);
        else
            self.Info.SetAtk(Model.Atk);

    }

    private void DefensePotion(bool isTurnOver)
    {
        Model.Def = 2;

        if (isTurnOver)
            self.Info.SetDef(-Model.Def);
        else
            self.Info.SetDef(Model.Def);
    }
}

public class ServerShopEffectStateModel
{
    public bool IsContinueRoll { get; set; }
    public double Hp { get; set; }
    public int BlockIndex { get; set; }
    public int LevelUp { get; set; }
    public bool IsHeal { get; set; }
    public double Atk { get; set; }
    public double Def { get; set; }
}