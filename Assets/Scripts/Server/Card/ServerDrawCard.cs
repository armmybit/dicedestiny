﻿using CI.TaskParallel;
using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class ServerDrawCard
{
    public List<ServerBuffInfoModel> cardsInuse = new List<ServerBuffInfoModel>();
    public List<ServerCardStack> cards = new List<ServerCardStack>();

    public void Create(ServerBuffModel model)
    {
        var acticeStack = new ServerCardActive();
        var triggerStack = new ServerCardTrigger();

        model.Data.ForEach(x =>
        {
            CardType typeCard = x.TypeCard;

            switch (typeCard)
            {
                case CardType.Active:

                    acticeStack.cards.Add(x);

                    break;
                case CardType.Trigger:

                    triggerStack.cards.Add(x);

                    break;
            }
        });

        acticeStack.Update();
        triggerStack.Update();

        this.cards.Add(acticeStack);
        this.cards.Add(triggerStack);

        this.cards = this.cards.OrderBy(o => Guid.NewGuid()).ToList();
    }

    public List<ServerBuffInfoModel> Draw(DrawCard draw)
    {
        var cardsDraw = new List<ServerBuffInfoModel>();

        switch (draw)
        {
            case DrawCard.One:

                var cardTypeOne = GetCardType > 50 ? 1 : 0;
                var cardStackOne = this.cards[cardTypeOne];
                var cardModelOne = cardStackOne.cards[0];

                this.cardsInuse.Add(cardModelOne);
                cardStackOne.Pop(cardModelOne);

                cardsDraw.Add(cardModelOne);

                break;
            case DrawCard.Two:

                var cardType1Two = GetCardType > 50 ? 1 : 0;
                var cardType2Two = GetCardType > 50 ? 1 : 0;

                var cardStack1Two = this.cards[cardType1Two];
                var cardStack2Two = this.cards[cardType2Two];

                var cardModel1Two = cardStack1Two.cards[0];
                var cardModel2Two = cardStack2Two.cards[0];

                this.cardsInuse.Add(cardModel1Two);
                this.cardsInuse.Add(cardModel2Two);

                cardStack1Two.Pop(cardModel1Two);
                cardStack1Two.Pop(cardModel2Two);

                cardsDraw.Add(cardModel1Two);
                cardsDraw.Add(cardModel2Two);

                break;
            case DrawCard.Ultimate:

                break;
        }

        return cardsDraw;
    }

    public ServerBuffInfoModel Draw()
    {
        var cardType = GetCardType > 50 ? 1 : 0;
        var cardStack = this.cards[cardType];
        var card = (ServerBuffInfoModel)cardStack.cards[0].Clone();

        this.cardsInuse.Add(card);
        cardStack.Pop(card);

        return card;
    }

    public void Return(int id, CardType typeCard)
    {
        this.cardsInuse.ForEach(x =>
        {
            if (x.Id == id)
            {
                switch (typeCard)
                {
                    case CardType.Active:
                        this.cards[0].cards.Add(x);
                        break;
                    case CardType.Trigger:
                        this.cards[1].cards.Add(x);
                        break;
                }

                this.cardsInuse.Remove(x);
            }
        });
    }

    private int GetCardType
    {
        get
        {
            return UnityEngine.Random.Range(0, 100);
        }
    }
}

[Serializable]
public class ServerCardTrigger : ServerCardStack
{
}

[Serializable]
public class ServerCardActive : ServerCardStack
{
}

[Serializable]
public class ServerCardStack
{
    public int number;
    public List<ServerBuffInfoModel> cards = new List<ServerBuffInfoModel>();

    public void Pop(ServerBuffInfoModel item)
    {
        this.cards.Remove(item);
        Update();
    }

    public void Update()
    {
        this.number = cards.Count;
    }
}