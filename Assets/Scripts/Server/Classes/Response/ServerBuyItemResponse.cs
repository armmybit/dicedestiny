﻿public class ServerBuyItemResponse
{
    public bool IsCancel { get; set; }
    public double Currency { get; set; }
    public ServerBuffInfoModel Buff { get; set; }
    public BuffState State { get; set; }
    public ServerShopEffectStateModel Effect { get; set; }
}