﻿using System.Collections.Generic;

public class ServerReceiveEventResponse
{
    public ServerBuffInfoModel Buff { get; set; }
    public BuffState State { get; set; }
    public ServerEventEffectStateModel Effect { get; set; }
}