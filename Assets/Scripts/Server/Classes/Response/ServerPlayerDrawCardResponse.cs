﻿
using System.Collections.Generic;

public class ServerPlayerDrawCardResponse
{
    public GameState GameState { get; set; }
    public List<ServerPlayerDrawCard> Players = new List<ServerPlayerDrawCard>();
}