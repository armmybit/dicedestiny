﻿
using System.Collections.Generic;

public class ServerEndGameResponse
{
    public int Winner { get; set; }
    public List<int> Loosers = new List<int>();
}