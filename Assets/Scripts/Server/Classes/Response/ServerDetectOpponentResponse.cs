﻿
using System.Collections.Generic;

public class ServerDetectOpponentResponse
{
    public ServerAttackResponse Opponent { get; set; }
    public ServerBlock BlockUpdate { get; set; }
}