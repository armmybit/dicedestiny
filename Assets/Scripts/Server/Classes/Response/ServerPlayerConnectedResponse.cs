﻿
public class ServerPlayerConnectedResponse
{
    public int PlayerId { get; set; }
    public bool IsSuccess { get; set; }
}