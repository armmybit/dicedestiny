﻿
public class ServerPlayerTurnResponse
{
    public GameState State { get; set; }
    public int Id { get; set; }
    public int Round { get; set; }
}