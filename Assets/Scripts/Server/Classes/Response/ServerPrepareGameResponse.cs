﻿
using System.Collections.Generic;

public class ServerPrepareGameResponse
{
    public GameState GameState { get; set; }
    public List<ServerBlock> Blocks = new List<ServerBlock>();
    public List<int> Orders = new List<int>();
}