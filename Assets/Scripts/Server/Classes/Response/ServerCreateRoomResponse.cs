﻿using System.Collections.Generic;

public class ServerCreateRoomResponse
{
    public string RoomId { get; set; }
    public List<ServerPlayerJoin> Players { get; set; }
}

public class ServerPlayerJoin
{
    public ServerPlayerProfile Profile { get; set; }
    public ServerPlayerStatusModel Status { get; set; }
    public PlayerFormBasementModel Basement { get; set; }
    public PlayerFormCharacterModel Main { get; set; }
    public PlayerFormCharacterModel Sub1 { get; set; }
    public PlayerFormCharacterModel Sub2 { get; set; }
}