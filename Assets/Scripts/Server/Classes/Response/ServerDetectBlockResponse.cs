﻿
using System.Collections.Generic;

public class ServerDetectBlockResponse
{
    public BlockType BlockType { get; set; }

    // Castle
    public int BaseLevel { get; set; }
    public double Currency { get; set; }

    // Event
    public ServerDetectBlockItemResponse EventItem { get; set; }

    // Shop
    public List<ServerDetectBlockItemResponse> ShopsItem = new List<ServerDetectBlockItemResponse>();

    // Flag
    public List<int> Targets = new List<int>();

    // Treasure

    // Monster
    public ServerBlock BlockUpdate { get; set; }
    public bool IsCreateNew { get; set; }
    public ServerMonster Monster { get; set; }
}

public class ServerDetectBlockItemResponse
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Effect { get; set; }
    public double Price { get; set; }
    public bool IsCanBuy { get; set; }
}