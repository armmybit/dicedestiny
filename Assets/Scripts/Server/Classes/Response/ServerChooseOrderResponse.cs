﻿
using System.Collections.Generic;

public class ServerChooseOrderResponse
{
    public GameState GameState { get; set; }
    public List<int> Numbers = new List<int>();
}