﻿using System.Collections.Generic;

public class ServerEndTurnResponse
{
    public int Id { get; set; }
    public int Round { get; set; }
    public ServerPlayerDrawCard Card { get; set; }
    public Dictionary<BuffType, List<ServerPlayerBuffOnGoing>> Buffs = new Dictionary<BuffType, List<ServerPlayerBuffOnGoing>>();
}

public class ServerPlayerDrawCard
{
    public int CardId { get; set; }
    public string Name { get; set; }
    public string Effect { get; set; }
    public Buff Type { get; set; }
    public CardType TypeCard { get; set; }
}

public class ServerPlayerBuffOnGoing
{
    public int Id { get; set; }
    public int RemainTurn { get; set; }
    public BuffState State { get; set; }
}