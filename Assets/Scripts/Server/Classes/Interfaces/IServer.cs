﻿using System.Collections;
using System.Collections.Generic;

public interface IServer
{
    // Create Room
    void CreateRoom(string reqStr);
    void OnCreateRoom(ServerRoom room);

    // Join Room
    void JoinRoom(string reqStr);
    void OnJoinRoom(bool isJoined);

    // Connect to Gameplay
    void PlayerConnectedToGamePlay(string reqStr);
    void OnPlayerConnectedToGamePlay(bool isSuccess);
    void CompletedPlayersConnected(GameState state, ServerRoom room);

    // State
    IEnumerator State(float deltaTime, GameState state, ServerRoom room = null);

    // Order
    void PlayerChooseOrder(string reqStr);
    void OnPlayerChooseOrder(GameState state, ServerRoom room);

    // Game Ready to start
    void PrepareGame(ServerRoom room);

    // Game Start
    void StartGame(GameState state, ServerRoom room);

    // Roll
    void Roll(string reqStr);
    void OnRoll(ServerDiceFace diceFace);

    // Player Stop at block
    void PlayerArrived(string reqStr);
    void DetectOpponent(ServerPlayerArrivedRequest req);
    void OnDetectOpponent(ServerAttackResponse opponent, ServerPlayer player);
    void DetectBlockType(ServerPlayerArrivedRequest req);
    void OnDetectBlockType(ServerDetectBlockResponse res);

    // Attack
    void Attack(string req);
    void OnPlayerAttack(ServerAttackResponse res);

    // Card
    List<ServerPlayerDrawCard> DrawCards(GameState state, ServerRoom room);

    // Shop
    void BuyItem(string reqStr);
    void OnBuyItem(ServerBuyItemResponse res);
    void OnCancelBuyItem();

    // Event
    void ReceiveEvent(string reqStr);
    void OnReceiveEvent(ServerReceiveEventResponse res);

    // End Turn
    void EndTurn(string reqStr);
    void NextTurn(int nextId, int round, Dictionary<BuffType, List<ServerPlayerBuffOnGoing>> buffs, ServerPlayerDrawCard card);

    // Game End
    void EndGame(ServerRoom room);
    void OnEndGame(int winner, List<int> loosers);
}