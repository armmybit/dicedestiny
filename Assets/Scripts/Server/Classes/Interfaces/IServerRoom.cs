﻿using System.Collections.Generic;

public interface IServerRoom
{
    void Setup(string roomId, ServerCreateRoomRequest req, ServerMapInfoModel mapInfo, List<ServerBuff> buffs);
    bool Join(ServerPlayerProfileModel model);
    ServerMonster Join(ServerMonsterProfileModel model, MonsterType monType);
    void JoinBot(List<ServerPlayerProfileModel> models);
    void SetPlayersBlock();
    bool Connected(int id);
    void SetOrder();
    List<int> GetOrders(int index, int id);
    ServerPlayer GetPlayer(int id);
    List<ServerPlayerJoin> GetPlayerJoin();
    List<int> GetPlayersOrder();
    ServerBlock GetBlock(ServerBlock info);
    ServerMonster GetMonster(int id);
    GameState ChangeGameState(GameState state);
    ServerPlayerDrawCard PlayerDrawCard(DrawCard draw, int playerId);
    List<ServerPlayerDrawCard> GetPlayerCard();
    int GetTurn();
    int GetRound();
    List<ServerBuffInfoModel> CardDraw(DrawCard drawCard);
    void ReturnCard(int id, CardType typeCard);
    ServerDiceFace RollDice(GameRoll roll);
    ServerAttackResponse DetectOpponent(int id, ServerBlock info);
    ServerMonster DetectMonster(ServerBlock info);
    ServerDetectBlockResponse DetectBlockType(int id, ServerBlock info);
    ServerAttackResponse PlayerAttack(int atkId, int defId, CharacterType defType);
    int MonsterAttack(int monsterId, int playerId);
    ServerBuyItemResponse BuyItem(int id, int shopId);
    ServerReceiveEventResponse ReceiveEvent(int id, int eventId);
    Dictionary<BuffType, List<ServerPlayerBuffOnGoing>> GetPlayerBuffs(int id);
    IServerBuff UseCard(int playerId, CardType typeCard, int cardId, int targetId);
    int GetNextPlayerTurn(int id);
    int CountPlayerAlive();
    int GetWinner();
    List<int> GetLoosers();
}