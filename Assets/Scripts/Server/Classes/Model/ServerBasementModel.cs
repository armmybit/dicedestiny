﻿using System;
using System.Collections.Generic;

[Serializable]
public class ServerBasementModel
{
    public List<ServerBasement> basements = new List<ServerBasement>();
}

[Serializable]
public class ServerBasement
{
    public int id;
    public string name;
}
