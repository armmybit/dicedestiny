﻿using System.Collections.Generic;

public class ServerMonsterModel
{
    public List<ServerMonsterTypeInfoModel> Data = new List<ServerMonsterTypeInfoModel>();
}

public class ServerMonsterTypeInfoModel
{
    public MonsterType MonsterType { get; set; }
    public List<ServerMonsterProfileModel> Collections = new List<ServerMonsterProfileModel>();
}

public class ServerMonsterProfileModel
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Level { get; set; }
    public ServerMonsterStatusModel Status { get; set; }
}

public class ServerMonsterStatusModel
{
    public double Hp { get; set; }
    public double Atk { get; set; }
    public MonsterAbility Status { get; set; }
}