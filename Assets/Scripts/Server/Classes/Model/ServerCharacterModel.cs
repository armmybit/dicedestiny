﻿using System;
using System.Collections.Generic;

[Serializable]
public class ServerCharacterModel
{
    public List<ServerCharacter> characters = new List<ServerCharacter>();
}

[Serializable]
public class ServerCharacter
{
    public int id;
    public string name;
    public List<ServerCharacterCollection> collections = new List<ServerCharacterCollection>();
}

[Serializable]
public class ServerCharacterCollection
{
    public int id;
    public string name;
}
