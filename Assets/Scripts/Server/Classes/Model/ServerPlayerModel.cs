﻿using System.Collections.Generic;

public class ServerPlayerModel
{
    public List<ServerPlayerProfileModel> Data = new List<ServerPlayerProfileModel>();
}

public class ServerPlayerProfileModel
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Level { get; set; }
    public ServerPlayerStatusModel Status { get; set; }
    public ServerPlayerJoinModel Join { get; set; }
}

public class ServerPlayerStatusModel
{
    public bool IsBot { get; set; }
    public double MaxHp { get; set; }
    public double Hp { get; set; }
    public double Atk { get; set; }
    public double Def { get; set; }
    public double Currency { get; set; }
}