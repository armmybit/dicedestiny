﻿using FullInspector;
using System.Collections.Generic;

public class ServerMapModel
{
    public List<ServerMapInfoModel> Data = new List<ServerMapInfoModel>();
}

public class ServerMapInfoModel
{
    public int Id { get; set; }
    public List<BlockType> Blocks = new List<BlockType>();
    public List<EventName> Events = new List<EventName>();
    public List<ShopName> Shops = new List<ShopName>();
}