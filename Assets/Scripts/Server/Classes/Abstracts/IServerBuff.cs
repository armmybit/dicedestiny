﻿using System;

[Serializable]
public abstract class IServerBuff
{
    public ServerBuffInfoModel buff;
    public ServerRoom room;
    public ServerPlayer self;
    public ServerPlayer playerTarget;
    public bool isGotEffect = false;
    public BuffState state;

    public virtual void Apply(ServerBuffInfoModel b, ref ServerRoom r, ref ServerPlayer s, ref ServerPlayer t)
    {
        buff = (ServerBuffInfoModel)b.Clone();
        room = r;
        self = s;
        playerTarget = t;

        isGotEffect = false;
        state = BuffState.NotReady;

        Use();
    }

    public virtual void Use()
    {
        if (buff.RemainUse == 0) // Ready to use
        {
            if (state != BuffState.NotReady) // Using
            {
                if (buff.RemainTurn > 0) // still on going
                {
                    Using();

                    if (buff.IsEffectiveEveryTurn) // Continue to give Buff or Debuff
                        Start();

                    buff.RemainTurn--;
                }
                else // Expire
                    End();
            }
            else // first time use
            {
                Start();
                buff.RemainTurn--;
            }
        }
        else // not ready to use
            buff.RemainUse--;
    }

    public virtual void Start()
    {
        state = BuffState.Ready;
    }

    private void Using()
    {
        state = BuffState.Using;
    }

    public virtual void End()
    {
        state = BuffState.Expired;
    }
}