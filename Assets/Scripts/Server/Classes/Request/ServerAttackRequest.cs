﻿
public class ServerAttackRequest : CommonRequest
{
    public int AttackerId { get; set; }
    public CharacterType AttackerType { get; set; }

    public int DefenderId { get; set; }
    public CharacterType DefenderType { get; set; }
}