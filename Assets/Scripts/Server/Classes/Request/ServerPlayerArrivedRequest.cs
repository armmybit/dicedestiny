﻿
public class ServerPlayerArrivedRequest : BasicRequest
{
    public BlockCheckState State { get; set; }
    public ServerBlock Info { get; set; }
}