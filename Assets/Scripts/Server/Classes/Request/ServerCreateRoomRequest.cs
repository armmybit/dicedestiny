﻿
public class ServerCreateRoomRequest
{
    public int MapId { get; set; }
    public int MaxPlayer { get; set; }
    public ServerPlayerJoinModel Player { get; set; }
}