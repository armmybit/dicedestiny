﻿
public class ServerPlayerConnectedRequest : CommonRequest
{
    public int PlayerId { get; set; }
}