﻿using System;
using System.Collections.Generic;

[Serializable]
public class ServerRoom : IServerRoom
{
    private const string TAG = "ServerRoom";

    public GameState GameState { get; set; }
    public string RoomId { get; set; }
    public int MaxPlayer { get; set; }
    public int MapId { get; set; }
    public bool IsGoAhead { get; set; }
    public ServerMapInfoModel Map { get; set; }
    public List<ServerPlayer> players = new List<ServerPlayer>();
    public List<ServerMonster> monsters = new List<ServerMonster>();
    public List<ServerBlock> blocks = new List<ServerBlock>();
    public List<ServerBuff> buffs = new List<ServerBuff>();
    public ServerTurn Turn { get; set; }
    public ServerRoll Roll { get; set; }
    public ServerDrawCard Card { get; set; }
    public ServerOrder Order { get; set; }

    #region Implement from IRoomServer

    public void Setup(string roomId, ServerCreateRoomRequest req, ServerMapInfoModel mapInfo, List<ServerBuff> buffs)
    {
        RoomId = roomId;
        MapId = req.MapId;
        MaxPlayer = req.MaxPlayer;
        IsGoAhead = true;
        Map = mapInfo;
        this.buffs = new List<ServerBuff>(buffs);

        Turn = new ServerTurn();
        Roll = new ServerRoll();
        Card = new ServerDrawCard();
        Order = new ServerOrder();

        // Player Detail
        ServerPlayerProfileModel player = SampleDB.GetServerPlayer(req.Player.Id);
        player.Join = req.Player;

        // Get Player Data
        var botPlayers = SampleDB.GetBots(3);

        Join(player);
        JoinBot(botPlayers);
        SetPlayersBlock();
        SetStackCards();
    }

    public bool Join(ServerPlayerProfileModel model)
    {
        try
        {
            this.players.Add(new ServerPlayer(model, CharacterType.Player));

            return true;
        }
        catch (Exception e)
        {
            JulietLogger.Error(TAG, "Error while player joining room " + e.Message);
        }

        return false;
    }

    public ServerMonster Join(ServerMonsterProfileModel model, MonsterType monType)
    {
        try
        {
            var monster = new ServerMonster(model, CharacterType.Monster, monType);
            this.monsters.Add(monster);

            return monster;
        }
        catch (Exception e)
        {
            JulietLogger.Error(TAG, "Error while monster joining room " + e.Message);
        }

        return null;
    }

    public void JoinBot(List<ServerPlayerProfileModel> models)
    {
        try
        {
            models.ForEach(bot =>
            {
                this.players.Add(new ServerPlayer(bot, CharacterType.Player));
            });
        }
        catch (Exception e)
        {
            JulietLogger.Error(TAG, "Error while bot joining room " + e.Message);
        }
    }

    public void SetOrder()
    {
        try
        {
            Order.CreateOrder(this.players.Count);
        }
        catch (Exception e)
        {
            JulietLogger.Error(TAG, "Error while setting orders " + e.Message);
        }
    }

    public List<int> GetOrders(int index, int id)
    {
        try
        {
            return GetPlayersOrder();
        }
        catch (Exception e)
        {
            JulietLogger.Error(TAG, "Error while getting orders " + e.Message);

            return null;
        }
    }

    public void SetPlayersBlock()
    {
        try
        {
            Turn.SetOrder(ref this.players);

            int index = 0;
            int indexBlock = 0;

            this.Map.Blocks.ForEach(x =>
            {
                ServerBlock block = new ServerBlock
                {
                    Index = indexBlock,
                    BlockType = x
                };

                if (block.BlockType == BlockType.Castle)
                {
                    var order = Turn.orders[index];
                    var player = this.players.Find(y => y.Info.Order == order);

                    player.Info.SetBreakPoints(block);
                    block.OwnerBlock = player.Profile.Id;
                    block.PlayersOnBlock.Add(player.Profile.Id);

                    index++;
                }

                this.blocks.Add(block);
                indexBlock++;
            });
        }
        catch (Exception e)
        {
            JulietLogger.Error(TAG, "Error while setting players block  " + e.Message);
        }
    }

    public void SetStackCards()
    {
        try
        {
            var data = this.buffs.Find(x => x.TypeBuff == BuffType.Card);
            Card.Create(data.Model);
        }
        catch (Exception e)
        {
            JulietLogger.Error(TAG, "Error while setting players block  " + e.Message);
        }
    }

    public bool Connected(int id)
    {
        try
        {
            this.players.Find(x => x.Profile.Id == id).Info.IsReady = true;

            return true;
        }
        catch (Exception e)
        {
            JulietLogger.Error(TAG, "Error while load to scene " + e.Message);
        }

        return false;
    }

    public ServerPlayer GetPlayer(int id)
    {
        try
        {
            return this.players.Find(x => x.Profile.Id == id);
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while getting player " + ex.Message);
        }

        return null;
    }

    public List<ServerPlayerJoin> GetPlayerJoin()
    {
        try
        {
            var playersJoin = new List<ServerPlayerJoin>();

            this.players.ForEach(x =>
            {
                var playerJoin = new ServerPlayerJoin()
                {
                    Profile = x.Profile,
                    Status = x.Info.Status,
                    Basement = x.Basement.Info,
                    Main = x.Character.main,
                    Sub1 = x.Character.sub1,
                    Sub2 = x.Character.sub2
                };

                playersJoin.Add(playerJoin);
            });

            return playersJoin;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while getting player " + ex.Message);
        }

        return null;
    }

    public int CheckPlayerConnected()
    {
        try
        {
            int connected = 0;

            this.players.ForEach(x =>
            {
                if (x.Info.IsReady)
                    connected++;
            });

            return connected;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while getting player " + ex.Message);
        }

        return -1;
    }

    public List<int> GetPlayersOrder()
    {
        try
        {
            var playersOrder = new List<int>();

            this.blocks.ForEach(x =>
            {
                if (x.BlockType == BlockType.Castle)
                    playersOrder.Add(x.OwnerBlock);
            });

            return playersOrder;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while getting player " + ex.Message);
        }

        return null;
    }

    public ServerBlock GetBlock(ServerBlock info)
    {
        try
        {
            return this.blocks.Find(x => x.Index == info.Index);
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while getting player " + ex.Message);
        }

        return null;
    }

    public ServerMonster GetMonster(int id)
    {
        try
        {
            return this.monsters.Find(x => x.Profile.Id == id && !x.Info.IsDied);
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while getting monster " + ex.Message);
        }

        return null;
    }

    public GameState ChangeGameState(GameState state)
    {
        return GameState = state;
    }

    public ServerPlayerDrawCard PlayerDrawCard(DrawCard draw, int playerId)
    {
        try
        {
            var player = GetPlayer(playerId);
            var card = Card.Draw();

            player.Card.Add(card);

            return new ServerPlayerDrawCard()
            {
                CardId = card.Id,
                Effect = card.Effect,
                Name = card.Name,
                TypeCard = card.TypeCard,
                Type = card.Type
            };
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while drawing card " + ex.Message);
        }

        return null;
    }

    public List<ServerPlayerDrawCard> GetPlayerCard()
    {
        try
        {
            var playersCard = new List<ServerPlayerDrawCard>();

            //this.players.ForEach(x =>
            //{
            //    var id = x.Profile.Id;
            //    var cards = x.Card.cards;

            //    playersCard.Add(new ServerPlayerDrawCard()
            //    {
            //        Id = id,
            //        Cards = cards
            //    });
            //});

            return playersCard;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while switching turn " + ex.Message);
        }

        return null;
    }

    public int GetTurn()
    {
        try
        {
            var order = Turn.Switch();
            var player = this.players.Find(x => x.Info.Order == order);

            player.Info.State = PlayerState.Start;

            return player.Profile.Id;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while switching turn " + ex.Message);
        }

        return -1;
    }

    public int GetRound()
    {
        return this.Turn.round;
    }

    public List<ServerBuffInfoModel> CardDraw(DrawCard drawCard)
    {
        try
        {
            return Card.Draw(drawCard);
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while getting card " + ex.Message);
        }

        return null;
    }

    public void ReturnCard(int id, CardType typeCard)
    {
        try
        {
            this.Card.Return(id, typeCard);
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while returning card " + ex.Message);
        }
    }

    public ServerDiceFace RollDice(GameRoll roll)
    {
        try
        {
            return (ServerDiceFace)Roll.Rolling(roll);
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while rolling dice " + ex.Message);
        }

        return null;
    }

    public ServerAttackResponse DetectOpponent(int id, ServerBlock info)
    {
        try
        {
            ServerAttackResponse res = null;
            var player = GetPlayer(id);
            var block = GetBlock(info);

            var playerBlock = this.blocks[player.Info.BreakPoints.Index];

            if (playerBlock.PlayersOnBlock.Contains(id))
                playerBlock.PlayersOnBlock.Remove(id);

            foreach (var opponentId in block.PlayersOnBlock)
            {
                if (opponentId != player.Profile.Id && !player.Info.IsDied)
                {
                    res = PlayerAttack(id, opponentId, CharacterType.Player);
                    break;
                }
            }

            block.PlayersOnBlock.Add(player.Profile.Id);

            player.Info.State = PlayerState.Battle;
            player.Info.SetBreakPoints(block);

            return res;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while detecting opponent " + ex.Message);
        }

        return null;
    }

    public ServerMonster DetectMonster(ServerBlock info)
    {
        try
        {
            var block = GetBlock(info);

            if (block.MonstersOnBlock.Count > 0)
            {
                var monster = GetMonster(block.MonstersOnBlock[0]);

                return monster;
            }

            return null;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while detecting monster " + ex.Message);
        }

        return null;
    }

    public ServerDetectBlockResponse DetectBlockType(int playerId, ServerBlock info)
    {
        try
        {
            var player = GetPlayer(playerId);
            var block = GetBlock(info);
            var res = new ServerDetectBlockResponse();

            switch (block.BlockType)
            {
                case BlockType.None:

                    block.BlockType = BlockType.None;

                    res = new ServerDetectBlockResponse()
                    {
                        BlockType = block.BlockType,
                    };

                    return res;

                case BlockType.Castle:

                    block.BlockType = BlockType.Castle;

                    if (playerId == block.OwnerBlock)
                    {
                        player.Basement.level += 1;
                        player.Info.SetCurrency(15);
                    }
                    else
                        player.Info.SetCurrency(-15);

                    res = new ServerDetectBlockResponse()
                    {
                        BlockType = block.BlockType,
                        BaseLevel = player.Basement.level,
                        Currency = player.Info.Status.Currency
                    };

                    return res;

                case BlockType.Flag:
                    block.BlockType = BlockType.Flag;


                    var targets = new List<int>();

                    this.players.ForEach(x =>
                    {
                        if (playerId != x.Profile.Id && !x.Info.IsDied)
                            targets.Add(x.Profile.Id);
                    });

                    res = new ServerDetectBlockResponse()
                    {
                        BlockType = block.BlockType,
                        Targets = targets
                    };

                    return res;

                case BlockType.Monster:
                    block.BlockType = BlockType.Monster;

                    var monster = DetectMonster(block);
                    bool isCreateNew = false;

                    if (monster == null)
                    {       
                        int round = GetRound();
                        int leftPlayer = this.players.Count;
                        var data = SampleDB.GetMonster(round, leftPlayer);

                        if (round <= 3)
                            monster = Join(data, MonsterType.LowTier);
                        else if (round <= 5)
                            monster = Join(data, MonsterType.MiddleTier);
                        else if (leftPlayer == 2) // In case of game almost over/ player 2 left
                            monster = Join(data, MonsterType.Mvp);
                        else
                            monster = Join(data, MonsterType.TopTier);

                        block.MonstersOnBlock.Add(monster.Profile.Id);
                        isCreateNew = true;

                        monster.Info.SetBreakPoints(block);
                    }

                    res = new ServerDetectBlockResponse()
                    {
                        BlockType = block.BlockType,
                        IsCreateNew = isCreateNew,
                        BlockUpdate = monster.Info.BreakPoints,
                        Monster = monster
                    };

                    return res;

                case BlockType.Event:
                    block.BlockType = BlockType.Event;

                    Map.Events.Shuffle();
                    var eventName = Map.Events[0];
                    var eventItems = this.buffs.Find(x => x.TypeBuff == BuffType.Event).Model;
                    var item = eventItems.Data.Find(x => (EventName)x.Id == eventName);

                    var events = new ServerDetectBlockItemResponse()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Effect = item.Effect
                    };

                    res = new ServerDetectBlockResponse()
                    {
                        BlockType = block.BlockType,
                        EventItem = events
                    };

                    return res;

                case BlockType.Shop:
                    block.BlockType = BlockType.Shop;

                    Map.Shops.Shuffle();
                    var shopNames = Map.Shops.GetRange(0, 3);
                    var shopItems = this.buffs.Find(x => x.TypeBuff == BuffType.Shop).Model;
                    var shops = new List<ServerDetectBlockItemResponse>();

                    shopItems.Data.ForEach(x =>
                    {
                        var shopName = (ShopName)x.Id;

                        if (shopName == shopNames[0] || shopName == shopNames[1] || shopName == shopNames[2])
                        {
                            int id = x.Id;
                            string name = x.Name;
                            string effect = x.Effect;
                            double price = x.Price;

                            var shopItem = new ServerDetectBlockItemResponse()
                            {
                                Id = id,
                                Name = name,
                                Effect = effect,
                                Price = price,
                                IsCanBuy = player.Info.Status.Currency >= price
                            };

                            shops.Add(shopItem);
                        }
                    });

                    res = new ServerDetectBlockResponse()
                    {
                        BlockType = block.BlockType,
                        ShopsItem = shops
                    };

                    return res;

                case BlockType.Treasure:
                    block.BlockType = BlockType.Treasure;

                    break;
            }

            player.Info.State = PlayerState.Event;

            return null;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while detecting block type " + ex.Message);
        }

        return null;
    }

    public ServerAttackResponse PlayerAttack(int atkId, int defId, CharacterType defType)
    {
        try
        {
            var atker = GetPlayer(atkId);
            double atkDmg;
            double atkDef;
            double atkNewDmg;
            bool isAtkDie = false;

            double defDmg;
            double defDef;
            double defNewDmg;
            bool isDefDie = false;

            switch (defType)
            {
                case CharacterType.Player:

                    var defPlayer = GetPlayer(defId);

                    // First Data
                    atkDmg = atker.Info.Status.Atk;
                    atkDef = atker.Info.Status.Def;

                    // Second Data
                    defDmg = defPlayer.Info.Status.Atk;
                    defDef = defPlayer.Info.Status.Def;

                    // First Atker new dmg
                    atkNewDmg = atkDmg - defDef;

                    // Second Atker new dmg
                    defNewDmg = defDmg - atkDef;

                    // Decrease Second Atker Hp
                    defPlayer.Info.SetHp(-atkNewDmg);

                    // Second Atker Still allive?
                    isDefDie = defPlayer.Info.IsDied;

                    // Gonna be revenge First Atker
                    if (!isDefDie)
                    {
                        // Decrease First Atker Hp
                        atker.Info.SetHp(-defNewDmg);
                        isAtkDie = atker.Info.IsDied;
                    }

                    if(isAtkDie)
                    {
                        var currentBP = GetBlock(atker.Info.BreakPoints);
                        atker.Info.SetBreakPoints(null);
                        this.blocks.Find(x => x.Index == currentBP.Index).PlayersOnBlock.Remove(atker.Profile.Id);
                    }

                    if(isDefDie)
                    {
                        var currentBP = GetBlock(defPlayer.Info.BreakPoints);
                        defPlayer.Info.SetBreakPoints(null);
                        this.blocks.Find(x => x.Index == currentBP.Index).PlayersOnBlock.Remove(defPlayer.Profile.Id);
                    }

                    var resPlayer = new ServerAttackResponse()
                    {
                        IsAttackerDie = isAtkDie,
                        AttkerHp = atker.Info.Status.Hp,
                        DefenderId = defId,
                        IsDefenderDie = isDefDie,
                        DefenderType = defType,
                        DefenderHp = defPlayer.Info.Status.Hp
                    };

                    return resPlayer;

                case CharacterType.Monster:

                    var defMonster = GetMonster(defId);

                    // First Data
                    atkDmg = atker.Info.Status.Atk;
                    atkDef = atker.Info.Status.Def;

                    // Second Data
                    defDmg = defMonster.Info.Status.Atk;
                    var monsterType = defMonster.Profile.MonsterType;
                    var monsterAbility = defMonster.Info.Status.Status;

                    switch (monsterAbility)
                    {
                        case MonsterAbility.None:

                            // Decrease Second Atker Hp
                            defMonster.Info.SetHp(-atkDmg);

                            break;
                        case MonsterAbility.Take1Damage:
                            defMonster.Info.SetHp(-1);
                            break;
                    }

                    // Second Atker Still allive?
                    isDefDie = defMonster.Info.IsDied;

                    // Gonna be revenge First Atker
                    if (!isDefDie)
                    {
                        // Second Atker new dmg
                        defNewDmg = defDmg - atkDef;

                        // Decrease First Atker Hp
                        atker.Info.SetHp(-defNewDmg);
                        isAtkDie = atker.Info.IsDied;
                    }
                    
                    if (isAtkDie)
                    {
                        var currentBP = GetBlock(atker.Info.BreakPoints);
                        atker.Info.SetBreakPoints(null);
                        this.blocks.Find(x => x.Index == currentBP.Index).PlayersOnBlock.Remove(atker.Profile.Id);
                    }

                    if (isDefDie)
                    {
                        var currentBP = GetBlock(defMonster.Info.BreakPoints);
                        defMonster.Info.SetBreakPoints(null);
                        this.blocks.Find(x => x.Index == currentBP.Index).MonstersOnBlock.Remove(defMonster.Profile.Id);
                        this.monsters.Remove(defMonster);
                    }

                    var resMonster = new ServerAttackResponse()
                    {
                        IsAttackerDie = isAtkDie,
                        AttkerHp = atker.Info.Status.Hp,
                        DefenderId = defId,
                        IsDefenderDie = isDefDie,
                        DefenderType = defType,
                        DefenderHp = defMonster.Info.Status.Hp
                    };

                    return resMonster;
            }
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while attacking opponent " + ex.Message);
        }

        return null;
    }

    public int PlayerAttack(int playerId, int targetId)
    {
        try
        {
            ServerPlayer player = GetPlayer(playerId);
            ServerPlayer playerTarget = GetPlayer(targetId);

            var cardTrigger = playerTarget.Card.Trigger();

            if (cardTrigger != null)
            {
                bool isPassAttack = playerTarget.Buff.ApplyTriggerCard(cardTrigger, this);

                if (!isPassAttack)
                {
                    double playerAtk = player.Info.Status.Atk;
                    double playerTargetDef = playerTarget.Info.Status.Def;
                    double difDamage = playerAtk - playerTargetDef;

                    playerTarget.Info.SetHp(-difDamage);

                    return 1;
                }
            }

            return 0;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while Attacking opponent " + ex.Message);
        }

        return -1;
    }

    public int MonsterAttack(int monsterId, int playerId)
    {
        try
        {
            ServerPlayer player = GetPlayer(monsterId);
            ServerPlayer playerTarget = GetPlayer(playerId);

            var cardTrigger = playerTarget.Card.Trigger();

            if (cardTrigger != null)
            {
                bool isPassAttack = playerTarget.Buff.ApplyTriggerCard(cardTrigger, this);

                if (!isPassAttack)
                {
                    double playerAtk = player.Info.Status.Atk;
                    double playerTargetDef = playerTarget.Info.Status.Def;
                    double difDamage = playerAtk - playerTargetDef;

                    playerTarget.Info.SetHp(-difDamage);

                    return 1;
                }
            }

            return 0;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while Attacking opponent " + ex.Message);
        }

        return -1;
    }

    public ServerBuyItemResponse BuyItem(int id, int shopId)
    {
        try
        {
            var player = GetPlayer(id);
            var buff = this.buffs.Find(x => x.TypeBuff == BuffType.Shop).Model.Data.Find(y => y.Id == shopId);

            double itemPrice = buff.Price;
            player.Info.SetCurrency(-itemPrice);
            var buffApplied = player.Buff.Apply(buff, this);

            var modelApplied = buffApplied.buff;
            var state = buffApplied.state;
            var effect = ((ServerShop)buffApplied).Model;

            var res = new ServerBuyItemResponse()
            {
                IsCancel = false,
                Buff = modelApplied,
                Effect = effect,
                State = state,
                Currency = player.Info.Status.Currency
            };

            return res;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while buying item " + ex.Message);
        }

        return null;
    }

    public ServerReceiveEventResponse ReceiveEvent(int id, int eventId)
    {
        try
        {
            var player = GetPlayer(id);
            var eventBuffs = this.buffs.Find(x => x.TypeBuff == BuffType.Event);
            eventBuffs.Model.Data.Shuffle();
            var buff = eventBuffs.Model.Data.Find(y => y.Id == eventId);

            var buffApplied = player.Buff.Apply(buff, this);

            var modelApplied = buffApplied.buff;
            var state = buffApplied.state;
            var effect = ((ServerEvent)buffApplied).Model;

            var res = new ServerReceiveEventResponse()
            {
                Buff = modelApplied,
                Effect = effect,
                State = state
            };

            return res;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while Receiving event buff " + ex.Message);
        }

        return null;
    }

    public Dictionary<BuffType, List<ServerPlayerBuffOnGoing>> GetPlayerBuffs(int id)
    {
        try
        {
            var player = GetPlayer(id);
            var allBuffs = player.Buff.Get();
            var playerBuffs = new Dictionary<BuffType, List<ServerPlayerBuffOnGoing>>();

            var shops = new List<ServerPlayerBuffOnGoing>();
            var cards = new List<ServerPlayerBuffOnGoing>();
            var events = new List<ServerPlayerBuffOnGoing>();
            var skills = new List<ServerPlayerBuffOnGoing>();

            allBuffs.ForEach(x =>
            {
                var buffType = x.buff.TypeBuff;
                var buffState = x.state;

                var buffOnGoing = new ServerPlayerBuffOnGoing()
                {
                    Id = x.buff.Id,
                    RemainTurn = x.buff.RemainTurn,
                    State = buffState
                };

                switch (buffType)
                {
                    case BuffType.Card:
                        cards.Add(buffOnGoing);
                        break;
                    case BuffType.Shop:
                        shops.Add(buffOnGoing);
                        break;
                    case BuffType.Event:
                        events.Add(buffOnGoing);
                        break;
                    case BuffType.Skill:
                        skills.Add(buffOnGoing);
                        break;
                }
            });

            playerBuffs.Add(BuffType.Card, cards);
            playerBuffs.Add(BuffType.Shop, shops);
            playerBuffs.Add(BuffType.Event, events);
            playerBuffs.Add(BuffType.Skill, skills);

            return playerBuffs;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while getting player buffs " + ex.Message);
        }

        return null;
    }

    public IServerBuff UseCard(int playerId, CardType typeCard, int cardId, int targetId)
    {
        try
        {
            var player = GetPlayer(playerId);
            var playerTarget = GetPlayer(targetId);
            var activeCard = player.Card.Active(cardId);

            return player.Buff.Apply(activeCard, this, playerTarget);

        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while using card " + ex.Message);
        }

        return null;
    }

    public int GetNextPlayerTurn(int id)
    {
        try
        {
            ServerPlayer player = GetPlayer(id);
            int nextPlayerId = -1;
            bool isFound = false;

            while (!isFound)
            {
                nextPlayerId = GetTurn();
                player = GetPlayer(nextPlayerId);

                if (!player.Info.IsDied)
                    isFound = true;
            }

            player.Info.State = PlayerState.End;
            
            return nextPlayerId;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while getting next turn " + ex.Message);
        }

        return -1;
    }

    public int CountPlayerAlive()
    {
        try
        {
            int alives = 0;

            this.players.ForEach(x =>
            {
                if (!x.Info.IsDied)
                    alives++;
            });

            return alives;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while counting player alive " + ex.Message);
        }

        return -1;

    }

    public int GetWinner()
    {
        try
        {
            int winnerId = this.players.Find(x => !x.Info.IsDied).Profile.Id;

            return winnerId;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while getting winner " + ex.Message);
        }

        return -1;
    }

    public List<int> GetLoosers()
    {
        try
        {
            var loosers = new List<int>();

            this.players.ForEach(x =>
            {
                if (x.Info.IsDied)
                    loosers.Add(x.Profile.Id);
            });


            return loosers;
        }
        catch (Exception ex)
        {
            JulietLogger.Error(TAG, "Error while getting loosers " + ex.Message);
        }

        return null;
    }

    #endregion
}