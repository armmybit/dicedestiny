﻿using System.Collections.Generic;

public class ServerBlock
{
    public int Index { get; set; }
    public BlockType BlockType { get; set; }
    public int OwnerBlock { get; set; }
    public List<int> PlayersOnBlock = new List<int>();
    public List<int> MonstersOnBlock = new List<int>();
}