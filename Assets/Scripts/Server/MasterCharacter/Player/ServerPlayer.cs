﻿
public class ServerPlayer : MasterCharacter
{
    public ServerPlayerProfile Profile { get; set; }
    public ServerPlayerInfo Info { get; set; }
    public ServerPlayerBasement Basement { get; set; }
    public ServerPlayerBuff Buff { get; set; }
    public ServerPlayerCharacterSlot Character { get; set; }
    public ServerPlayerCard Card { get; set; }

    public ServerPlayer(ServerPlayerProfileModel player, CharacterType characterType)
    {
        CharacterType = characterType;
        Profile = new ServerPlayerProfile(player);
        Info = new ServerPlayerInfo(player);
        Basement = new ServerPlayerBasement(player.Join.Basement);
        Buff = new ServerPlayerBuff();
        Character = new ServerPlayerCharacterSlot(player.Join);
        Card = new ServerPlayerCard();

        Buff.Init(this);
        Basement.Init(this);
        Character.Init(this);
    }
}