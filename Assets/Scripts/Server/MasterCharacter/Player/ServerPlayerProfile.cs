﻿
public class ServerPlayerProfile
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Level { get; set; }

    public ServerPlayerProfile(ServerPlayerProfileModel player)
    {
        Id = player.Id;
        Name = player.Name;
        Level = player.Level;
    }
}