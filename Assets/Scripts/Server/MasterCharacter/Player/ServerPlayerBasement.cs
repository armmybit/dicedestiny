﻿
public class ServerPlayerBasement
{
    public ServerPlayer Self { get; set; }
    public PlayerFormBasementModel Info { get; set; }
    public int level;
    public int blockId;

    public ServerPlayerBasement(PlayerFormBasementModel basement)
    {
        Info = basement;
        this.level = 1;
    }

    public void Init(ServerPlayer self)
    {
        Self = self;
    }

    public void SetBlockId(int id)
    {
        this.blockId = id;
    }
}