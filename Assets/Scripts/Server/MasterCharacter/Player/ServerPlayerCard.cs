﻿using System;
using System.Collections.Generic;

[Serializable]
public class ServerPlayerCard
{
    public List<ServerBuffInfoModel> cards = new List<ServerBuffInfoModel>();

    public void Add(List<ServerBuffInfoModel> cards)
    {
        this.cards.AddRange(cards);
    }
    
    public void Add(ServerBuffInfoModel card)
    {
        this.cards.Add(card);
    }

    public ServerBuffInfoModel Active(int id)
    {
        var card = this.cards.Find(x => x.Id == id);
        this.cards.Remove(card);

        return card;
    }

    public ServerBuffInfoModel Trigger()
    {
        if (this.cards.Count > 0)
        {
            var card = this.cards.Find(x => x.TypeCard == CardType.Trigger);

            if (card != null)
                this.cards.Remove(card);

            return card;
        }

        return null;
    }
}