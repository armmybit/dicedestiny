﻿using System;
using System.Collections.Generic;
using CI.TaskParallel;

[Serializable]
public class ServerPlayerBuff
{
    public ServerPlayer self;
    public List<IServerBuff> iServerBuffs = new List<IServerBuff>();

    public void Init(ServerPlayer self)
    {
        this.self = self;
    }

    public IServerBuff Apply(ServerBuffInfoModel buff, ServerRoom room, ServerPlayer playerTarget = null)
    {
        IServerBuff iBuff = null;

        switch (buff.TypeBuff)
        {
            case BuffType.Card:
                iBuff = new ServerCard();
                break;
            case BuffType.Shop:
                iBuff = new ServerShop();
                break;
            case BuffType.Event:
                iBuff = new ServerEvent();
                break;
            case BuffType.Skill:
                break;
        }

        bool isExist = this.iServerBuffs.Exists(x => x.buff.Id == buff.Id);

        if (!isExist)
        {
            iBuff.Apply(buff, ref room, ref this.self, ref playerTarget);
            this.iServerBuffs.Add(iBuff);
        }
        else
        {
            var oldBuff = this.iServerBuffs.Find(x => x.buff.Id == buff.Id);
            oldBuff.Apply(buff, ref room, ref this.self, ref playerTarget);
        }

        //if (iBuff.buff.IsImmediateDestroy)
        //    this.iServerBuffs.Remove(iBuff);

        return iBuff;
    }

    public bool ApplyTriggerCard(ServerBuffInfoModel buff, ServerRoom room)
    {


        return true;
    }

    public List<IServerBuff> Get()
    {
        // Get all available buff/debuff
        var allBuffs = UnityTask.Run(() =>
        {
            var iBuffs = new List<IServerBuff>();

            this.iServerBuffs.ForEach(x =>
            {
                x.Use();
                iBuffs.Add(x);
            });

            iBuffs.ForEach(x =>
            {
                if (x.state == BuffState.Expired)
                    this.iServerBuffs.Remove(x);
            });

            return iBuffs;
        });

        UnityTask.WaitAll(allBuffs);

        return allBuffs.Result;
    }
}