﻿
public class ServerPlayerCharacterSlot
{
    public ServerPlayer Self { get; set; }
    public PlayerFormCharacterModel main;
    public PlayerFormCharacterModel sub1;
    public PlayerFormCharacterModel sub2;

    public ServerPlayerCharacterSlot(ServerPlayerJoinModel model)
    {
        this.main = model.Main;
        this.sub1 = model.Sub1;
        this.sub2 = model.Sub2;
    }

    public void Init(ServerPlayer self)
    {
        Self = self;
    }
}