﻿
public class ServerPlayerInfo
{
    // State
    public PlayerState State { get; set; }
    public ServerBlock BreakPoints { get; set; }
    public ServerPlayerStatusModel Status { get; set; }

    // Data
    public bool IsReady { get; set; }
    public int Order { get; set; }

    public ServerPlayerInfo(ServerPlayerProfileModel player)
    {
        Status = player.Status;

        if (Status.IsBot)
            IsReady = true;
    }

    public void SetBreakPoints(ServerBlock newBreakPoints)
    {
        BreakPoints = newBreakPoints;
    }

    public void SetCurrency(double currency)
    {
        Status.Currency += currency;

        if (Status.Currency < 0)
            Status.Currency = 0;
    }

    public void SetHp(double hp)
    {
        Status.Hp += hp;

        if (Status.Hp < 0)
            Status.Hp = 0;
    }

    public void SetAtk(double atk)
    {
        Status.Atk += atk;

        if (Status.Atk < 0)
            Status.Atk = 0;
    }

    public void SetDef(double def)
    {
        Status.Def += def;

        if (Status.Def < 0)
            Status.Def = 0;
    }

    public bool IsDied
    {
        get
        {
            if (Status.Hp <= 0)
                return true;
            return false;
        }
    }
}