﻿
public class ServerMonsterInfo
{
    // State
    public PlayerState State { get; set; }
    public ServerBlock BreakPoints { get; set; }
    public ServerMonsterStatusModel Status { get; set; }

    public ServerMonsterInfo(ServerMonsterStatusModel status)
    {
        Status = status;
    }

    public void SetBreakPoints(ServerBlock newBreakPoints)
    {
        BreakPoints = newBreakPoints;
    }

    public void SetHp(double hp)
    {
        Status.Hp += hp;

        if (Status.Hp < 0)
            Status.Hp = 0;
    }

    public void SetAtk(double atk)
    {
        Status.Atk += atk;

        if (Status.Atk < 0)
            Status.Atk = 0;
    }

    public bool IsDied
    {
        get
        {
            if (Status.Hp <= 0)
                return true;
            return false;
        }
    }
}