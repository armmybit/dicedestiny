﻿
public class ServerMonsterProfile
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Level { get; set; }
    public MonsterType MonsterType { get; set; }

    public ServerMonsterProfile(ServerMonsterProfileModel profile, MonsterType monType)
    {
        MonsterType = monType;
        Id = profile.Id;
        Name = profile.Name;
        //Level = profile.Level;
    }
}