﻿
public class ServerMonster : MasterCharacter
{
    public ServerMonsterProfile Profile { get; set; }
    public ServerMonsterInfo Info { get; set; }

    public ServerMonster(ServerMonsterProfileModel model, CharacterType characterType, MonsterType monsterType)
    {
        Profile = new ServerMonsterProfile(model, monsterType);
        Info = new ServerMonsterInfo(model.Status);
    }
}