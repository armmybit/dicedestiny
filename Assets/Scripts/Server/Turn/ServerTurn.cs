﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class ServerTurn
{
    public List<int> orders = new List<int>();
    public int index = 0;
    public int round = 0;

    public ServerTurn()
    {
        this.index = 0;
        this.round = 0;
        this.orders = new List<int>();
    }

    public void SetOrder(ref List<ServerPlayer> players)
    {
        List<int> orderNumbers = new List<int>();

        int order = 1;

        for (; order <= players.Count; order++)
            orderNumbers.Add(order);

        var newOrders = orderNumbers.OrderBy(o => Guid.NewGuid());

        int i = 0;

        foreach (var newOrder in newOrders)
        {
            ServerPlayer player = players[i];
            player.Info.Order = newOrder;
            i++;

            this.orders.Add(newOrder);
        }

        this.orders = this.orders.OrderBy(x => x).ToList();
    }

    public void SetOrder(ref List<ServerPlayer> players, int index, int playerId)
    {
        List<int> orderNumbers = new List<int>();

        int order = 1;

        for (; order <= players.Count; order++)
            orderNumbers.Add(order);

        var newOrders = orderNumbers.OrderBy(o => Guid.NewGuid());

        int i = 0;

        foreach (var newOrder in newOrders)
        {
            ServerPlayer player = players[i];
            player.Info.Order = newOrder;
            i++;

            this.orders.Add(newOrder);
        }

        this.orders = this.orders.OrderBy(x => x).ToList();
    }

    public int Switch()
    {
        int latestIndex = this.index++ % this.orders.Count;

        if (latestIndex == 0)
            this.round++;

        return this.orders[latestIndex];
    }
}