﻿using System;
using System.Collections.Generic;

public class ServerRoll
{
    private const string TAG = "Roll";

    public Dictionary<int, int[]> Rolls = new Dictionary<int, int[]>();

    public ServerRoll()
    {
        Rolls = new Dictionary<int, int[]>() {
        { 0, new int[3]{ 2, 3, 4 } },
        { 1, new int[4]{ 5, 6, 7, 8 } },
        { 2, new int[4]{ 9, 10, 11, 12 } }
        };
    }

    public ServerDiceFace Rolling(GameRoll r)
    {
        try
        {
            int[] numberSet = Rolls[(int)r];
            int drawNumber = numberSet[UnityEngine.Random.Range(0, numberSet.Length)] - 1;

            int[] numberSeries = new int[drawNumber];

            for (int i = 0; i < numberSeries.Length; i++)
            {
                numberSeries[i] = i + 1;
            }

            List<ServerDiceFace> dices = new List<ServerDiceFace>();

            if (numberSeries.Length > 1)
            {
                int midIndex = numberSeries.Length % 2 == 0 ? numberSeries.Length / 2 : (numberSeries.Length / 2) + 1;

                ServerDiceFace df;

                for (int i = 0; i < midIndex; i++)
                {
                    if (UnityEngine.Random.value > 0.5f)
                    {
                        df = new ServerDiceFace()
                        {
                            Number1 = numberSeries[i],
                            Number2 = numberSeries[numberSeries.Length - 1 - i]
                        };
                    }
                    else
                    {
                        df = new ServerDiceFace()
                        {
                            Number1 = numberSeries[numberSeries.Length - 1 - i],
                            Number2 = numberSeries[i]
                        };
                    }

                    dices.Add(df);
                }
            }
            else
            {
                dices.Add(
                    new ServerDiceFace()
                    {
                        Number1 = numberSeries[0],
                        Number2 = numberSeries[0]
                    }
                );
            }

            dices.Shuffle();

            ServerDiceFace dice = dices[0];
            int diffValue = 0;

            if (dice.Number1 > 6)
            {
                diffValue = dice.Number1 - 6;
                dice.Number1 -= diffValue;
                dice.Number2 += diffValue;
            }

            if (dice.Number2 > 6)
            {
                diffValue = dice.Number2 - 6;
                dice.Number2 -= diffValue;
                dice.Number1 += diffValue;
            }

            return dice;
        }
        catch (System.Exception ex)
        {
            JulietLogger.Error(TAG, "Error while finding number for dice " + ex);
            return null;
        }
    }
}