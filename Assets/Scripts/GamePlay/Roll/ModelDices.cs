﻿using System.Collections;
using UnityEngine;

public class ModelDices : MonoBehaviour
{
    [Header("Dice")]
    public Dice dice1;
    public Dice dice2;

    [Header("GameObject")]
    public GameObject container;

    public void Open(DiceFace face)
    {
        this.container.SetActive(true);

        SetDice(face.Number1, face.Number2);
        StartCoroutine("ThrowDice");
    }

    private void Close()
    {
        this.dice1.Clear();
        this.dice2.Clear();
        this.container.SetActive(false);
    }

    private void SetDice(int number1, int number2)
    {
        this.dice1.Play(number1);
        this.dice2.Play(number2);
    }

    private IEnumerator ThrowDice()
    {
        yield return new WaitForSeconds(2f);
        Close();
    }
}