﻿using System;
using System.Collections;
using UnityEngine;

public class BlockUpgrade : MonoBehaviour
{
    public Action Callback;

    public void Open()
    {
        StartCoroutine("Upgrading");
    }

    private IEnumerator Upgrading()
    {
        yield return new WaitForSeconds(2f);

        Done();
    }

    public void Done()
    {
        Callback();
    }
}