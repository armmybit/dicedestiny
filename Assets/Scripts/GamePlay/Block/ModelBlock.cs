﻿using FullInspector;
using System;
using UnityEngine;

public class ModelBlock : BaseBehavior<JsonNetSerializer>
{
    // Baisc
    public Block Info { get; set; }
    public BlockUpgrade Upgrade { get; set; }

    public GameObject blockNone;
    public GameObject blockCastle;
    public GameObject blockFlag;
    public GameObject blockMonster;
    public GameObject blockEvent;
    public GameObject blockShop;

    public int remainStep;
    private Action<int> ContinueMove;

    // UI
    public TextMesh levelText;

    public void Init(Block info)
    {
        Info = info;
        Upgrade = GetComponent<BlockUpgrade>();

        blockNone.SetActive(false);

        switch (info.BlockType)
        {
            case BlockType.None:
                blockNone.SetActive(true);
                break;
            case BlockType.Castle:
                blockCastle.SetActive(true);
                break;
            case BlockType.Flag:
                blockFlag.SetActive(true);
                break;
            case BlockType.Monster:
                blockMonster.SetActive(true);
                break;
            case BlockType.Event:
                blockEvent.SetActive(true);
                break;
            case BlockType.Shop:
                blockShop.SetActive(true);
                break;
        }
    }

    public void LevelIncrease(int level)
    {
        Upgrade.Callback = OnUpgraded;
        Upgrade.Open();
    }

    public void OnUpgraded()
    {
        if (remainStep == 0)
            GamePlayController.Instance.CurrentPlayer.EndTurn();
        else
        {
            int tmpRemainStep = remainStep;
            remainStep = 0;
            ContinueMove(tmpRemainStep);
        }
    }

    public void PlayerArrived(ModelPlayer player)
    {
        GamePlayController.Instance.PlayerArrived(this.Info);
    }

    public bool IsBlockCastle(int remainingStep, Action<int> Callback)
    {
        if (Info.BlockType == BlockType.Castle && GamePlayController.Instance.CurrentPlayer.Data.Profile.Id == Info.OwnerBlock)
        {
            remainStep = remainingStep;
            ContinueMove = Callback;

            return true;
        }

        return false;
    }
}
