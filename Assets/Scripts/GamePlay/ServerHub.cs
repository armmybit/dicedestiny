﻿using Newtonsoft.Json;
using System.Threading.Tasks;
using UnityEngine;

public static class ServerHub
{
    private const string TAG = "ServerHub";

    public static void CreateRoom(CreateRoomRequest req)
    {
        JulietLogger.Info(TAG, "CreateRoom");

        var reqStr = JsonConvert.SerializeObject(req);

        JulietLogger.Info(TAG, reqStr);

        ServerController.Instance.CreateRoom(reqStr);
    }

    public static void OnCreateRoom(string resStr)
    {
        JulietLogger.Info(TAG, "OnCreateRoom " + resStr);

        var res = JsonConvert.DeserializeObject<CreateRoomResponse>(resStr);

        CustomMatchController.Instance.OnCreateRoom(res);
    }

    public static void PlayerConnected(PlayerConnectedRequest req)
    {
        JulietLogger.Info(TAG, "PlayerConnected");

        var reqStr = JsonConvert.SerializeObject(req);

        JulietLogger.Info(TAG, reqStr);

        ServerController.Instance.PlayerConnectedToGamePlay(reqStr);
    }

    public static void OnPlayerConnected(string resStr)
    {
        JulietLogger.Info(TAG, "OnPlayerConnected " + resStr);

        var res = JsonConvert.DeserializeObject<PlayerConnectedResponse>(resStr);

        GamePlayController.Instance.OnPlayerConnectedToGamePlay(res);
    }

    public static void OnCompletedPlayersConnected(string resStr)
    {
        JulietLogger.Info(TAG, "OnCompletedPlayersConnected " + resStr);

        var res = JsonConvert.DeserializeObject<ConnectedPlayersResponse>(resStr);

        GamePlayController.Instance.OnCompletedPlayersConnected(res);
    }

    public static void PlayerChooseOrder(ChooseOrderRequest req)
    {
        JulietLogger.Info(TAG, "PlayerChooseOrder");

        var reqStr = JsonConvert.SerializeObject(req);

        JulietLogger.Info(TAG, reqStr);

        ServerController.Instance.PlayerChooseOrder(reqStr);
    }

    public static void OnPlayerChooseOrder(string resStr)
    {
        JulietLogger.Info(TAG, "OnPlayerChooseOrder " + resStr);

        var res = JsonConvert.DeserializeObject<ChooseOrderResponse>(resStr);

        GamePlayController.Instance.OnPlayerChooseOrder(res);
    }

    public static void OnPrepareGame(string resStr)
    {
        JulietLogger.Info(TAG, "PrepareGame " + resStr);

        var res = JsonConvert.DeserializeObject<PrepareGameResponse>(resStr);

        GamePlayController.Instance.PrepareGame(res);
    }

    public static void UseCard(UseCardRequest req)
    {
        JulietLogger.Info(TAG, "UseCard ");

        var reqStr = JsonConvert.SerializeObject(req);

        JulietLogger.Info(TAG, reqStr);


    }

    public static void StartGame(string resStr)
    {
        JulietLogger.Info(TAG, "StartGame " + resStr);

        var res = JsonConvert.DeserializeObject<StartGameResponse>(resStr);

        GamePlayController.Instance.StartGame(res);
    }

    public static void Roll(RollRequest req)
    {
        JulietLogger.Info(TAG, "Roll");

        var reqStr = JsonConvert.SerializeObject(req);

        JulietLogger.Info(TAG, reqStr);

        ServerController.Instance.Roll(reqStr);
    }

    public static void OnRoll(string resStr)
    {
        JulietLogger.Info(TAG, "OnRoll " + resStr);

        var res = JsonConvert.DeserializeObject<RollResponse>(resStr);

        GamePlayController.Instance.OnRoll(res);
    }

    public static void PlayerArrived(PlayerArrivedRequest req)
    {
        JulietLogger.Info(TAG, "PlayerArrived");

        var reqStr = JsonConvert.SerializeObject(req);

        JulietLogger.Info(TAG, reqStr);

        ServerController.Instance.PlayerArrived(reqStr);
    }

    public static void OnDetectOpponent(string resStr)
    {
        JulietLogger.Info(TAG, "OnDetectOpponent " + resStr);

        var res = JsonConvert.DeserializeObject<DetectOpponentResponse>(resStr);

        GamePlayController.Instance.OnDetectOpponent(res);
    }

    public static void OnDetectBlockType(string resStr)
    {
        JulietLogger.Info(TAG, "OnDetectBlockType " + resStr);

        var res = JsonConvert.DeserializeObject<DetectBlockResponse>(resStr);

        GamePlayController.Instance.OnDetectBlock(res);
    }

    public static void Attack(AttackRequest req)
    {
        JulietLogger.Info(TAG, "Attack");

        var reqStr = JsonConvert.SerializeObject(req);

        JulietLogger.Info(TAG, reqStr);

        ServerController.Instance.Attack(reqStr);
    }

    public static void OnPlayerAttack(string resStr)
    {
        JulietLogger.Info(TAG, "OnPlayerAttack " + resStr);

        var res = JsonConvert.DeserializeObject<AttackResponse>(resStr);

        GamePlayController.Instance.OnPlayerAttack(res);
    }

    public static void ReceiveEvent(ReceiveEventRequest req)
    {
        JulietLogger.Info(TAG, "ReceiveEvent");

        var reqStr = JsonConvert.SerializeObject(req);

        JulietLogger.Info(TAG, reqStr);

        ServerController.Instance.ReceiveEvent(reqStr);
    }

    public static void OnReceiveEvent(string resStr)
    {
        JulietLogger.Info(TAG, "OnReceiveEvent " + resStr);

        var res = JsonConvert.DeserializeObject<ReceiveEventResponse>(resStr);

        GamePlayController.Instance.OnReceiveEvent(res);
    }

    public static void BuyItem(BuyItemRequest req)
    {
        JulietLogger.Info(TAG, "BuyItem");

        var reqStr = JsonConvert.SerializeObject(req);

        JulietLogger.Info(TAG, reqStr);

        ServerController.Instance.BuyItem(reqStr);
    }

    public static void OnBuyItem(string resStr)
    {
        JulietLogger.Info(TAG, "OnBuyItem " + resStr);

        var res = JsonConvert.DeserializeObject<BuyItemResponse>(resStr);

        GamePlayController.Instance.OnBuyItem(res);
    }

    public static void EndTurn(EndTurnRequest req)
    {
        JulietLogger.Info(TAG, "EndTurn");

        var reqStr = JsonConvert.SerializeObject(req);

        JulietLogger.Info(TAG, reqStr);

        ServerController.Instance.EndTurn(reqStr);
    }

    public static void OnNextTurn(string resStr)
    {
        JulietLogger.Info(TAG, "OnNextTurn " + resStr);

        var res = JsonConvert.DeserializeObject<EndTurnResponse>(resStr);

        GamePlayController.Instance.OnEndTurn(res);
    }

    public static void OnEndGame(string resStr)
    {
        JulietLogger.Info(TAG, "OnEndGame " + resStr);

        var res = JsonConvert.DeserializeObject<EndGameResponse>(resStr);

        GamePlayController.Instance.OnEndGame(res);
    }
}