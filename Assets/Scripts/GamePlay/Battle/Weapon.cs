﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
    bool canAttack;

    BattleMovement self;

    void Awake()
    {
        self = GetComponentInParent<BattleMovement>();
    }

    public void SetAttack()
    {
        canAttack = true;
    }

    public void ResetAttack()
    {
        canAttack = false;
    }

    void OnTriggerStay(Collider obj)
    {
        if (obj.GetComponent<BattleMovement>() && obj.GetComponent<BattleMovement>() != self)
        {
            if (canAttack && self.IsDuringAttack())
            {
                canAttack = false;
                obj.GetComponent<BattleMovement>().TakeDamage();
            }
        }
    }


}
