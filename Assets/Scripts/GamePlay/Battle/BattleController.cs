﻿using System;
using System.Collections;
using UnityEngine;

public class BattleController : MonoBehaviour
{
    public BattlePointHandler[] battlePoints;

    BattleMovement player;
    BattleMovement enemy;

    Vector3 lastPlayerPos;
    Vector3 lastEnemyPos;

    public BlockCheckState state;

    private static BattleController _instance;

    public static BattleController Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;

        battlePoints = FindObjectsOfType<BattlePointHandler>();
    }

    // Short Attack
    public void Short(BattleMovement player, BattleMovement enemy, BlockCheckState state)
    {
        lastPlayerPos = player.transform.position;
        lastEnemyPos = enemy.transform.position;

        this.player = player;
        this.enemy = enemy;
        this.state = state;

        BattleTransitionController.Instance.Play(OnInitBattle);
    }

    void OnInitBattle()
    {
        player.WarpToPosition(battlePoints[0].transform.position);
        player.SetEnemy(enemy);
        enemy.WarpToPosition(battlePoints[1].transform.position);
        enemy.SetEnemy(player);

        StartCoroutine(ReadyToBattle(player, enemy, OnPlayerEndAction, 1));
    }

    IEnumerator ReadyToBattle(BattleMovement attacker, BattleMovement target, System.Action<bool> callback, float time)
    {
        WaitForSeconds delay = new WaitForSeconds(time);
        yield return delay;

        attacker.MoveTowardsAndAttack(target, callback);
    }

    public void OnPlayerEndAction(bool isDie)
    {
        player.EndState();

        if (isDie)
            StartCoroutine(EndBattle());
        else
            StartCoroutine(ReadyToBattle(enemy, player, OnEnemyEndAction, .5f));
    }

    public void OnEnemyEndAction(bool isDie = false)
    {
        enemy.EndState();
        StartCoroutine(EndBattle());
    }

    IEnumerator EndBattle()
    {
        yield return new WaitForSeconds(2f);

        player.WarpToPosition(lastPlayerPos);
        enemy.WarpToPosition(lastEnemyPos);

        player.EndBattle();
        enemy.EndBattle();

        GamePlayController.Instance.OnBattleEnd(this.state);
    }

    // Remote Attack
    public void Remote(BattleMovement player, BattleMovement enemy, BlockCheckState state)
    {
        this.player = player;
        this.enemy = enemy;
        this.state = state;

        StartRemoteAttack();
    }

    private void StartRemoteAttack()
    {
        int atkIndex = GamePlayController.Instance.players.IndexOf(this.player.GetComponent<ModelPlayer>());
        int defIndex = GamePlayController.Instance.players.IndexOf(this.enemy.GetComponent<ModelPlayer>());

        StartCoroutine(CameraController.Instance.Focus(0f, this.player.transform, -1));
        StartCoroutine(CameraController.Instance.Focus(2f, this.enemy.transform, defIndex));
        StartCoroutine(CameraController.Instance.EndFocus(4f, atkIndex));
        StartCoroutine(EndRemoteAttack(5f));
    }

    private IEnumerator EndRemoteAttack(float deltaTime)
    {
        yield return new WaitForSeconds(deltaTime);

        GamePlayController.Instance.CurrentPlayer.EndTurn();
    }
}