﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BattleTransitionController : MonoBehaviour
{
    public Texture2D testTex;

    public Image imageTransition;
    public System.Action callback;

    private static BattleTransitionController _instance;

    public static BattleTransitionController Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
    }

    public void Play(System.Action callback = null)
    {
        this.callback = callback;
        StartCoroutine(TakeScreenshot());
    }

    IEnumerator TakeScreenshot()
    {
        yield return new WaitForEndOfFrame();
        Texture2D tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);
        tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        tex.Apply();

        testTex = tex;

        Sprite sp = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

        imageTransition.gameObject.SetActive(true);
        imageTransition.sprite = sp;

        StartCoroutine(Transition());
    }

    IEnumerator Transition()
    {
        float time = 1f;
        float exitTime = 0;

        float value = 0;

        while (exitTime < time)
        {
            value = Mathf.Lerp(0, 1, exitTime / time);

            imageTransition.material.SetFloat("_Cutoff", value);

            exitTime += Time.deltaTime;
            yield return null;
        }


        imageTransition.gameObject.SetActive(false);
        callback();
    }

}
