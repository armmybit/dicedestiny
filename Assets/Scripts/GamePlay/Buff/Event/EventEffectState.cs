﻿using System;
using System.Collections.Generic;

[Serializable]
public class EventEffectState : IBuff
{
    private const string TAG = "EventEffectState";

    public EventEffectStateModel Model { get; set; }

    protected void Set(EventEffectStateModel effect)
    {
        Model = effect;
        Effect();
    }

    protected void Update()
    {
        Effect();
    }

    private void Effect()
    {
        EventName name = (EventName)buff.Id;

        switch (name)
        {
            case EventName.AmorBreak:
                AmorBreak(false);
                break;
            case EventName.WeaponBreak:
                WeaponBreak(false);
                break;
            case EventName.Damage:
                Damage(false);
                break;
            case EventName.MonsterInvades:
                MonsterInvades(1, 4); // TODO
                break;
            case EventName.Heal:
                Heal();
                break;
            case EventName.DamageUp:
                DamageUp(false);
                break;
            case EventName.DefenseUp:
                DefenseUp(false);
                break;
            case EventName.MeteorStrike:
                MeteorStrike();
                break;
            case EventName.StormGust:
                StormGust(false);
                break;
            case EventName.ReStart_Teleport:
                Restart();
                break;
            case EventName.Reverse:
                Reverse();
                break;
            case EventName.Slow:
                Slow();
                break;
            case EventName.SpeedBoost:
                SpeedBoost();
                break;
        }
    }

    protected void Restore(int id)
    {
        EventName name = (EventName)id;

        switch (name)
        {
            case EventName.AmorBreak:

                if (buff.IsReset)
                    AmorBreak(true);

                break;
            case EventName.WeaponBreak:

                if (buff.IsReset)
                    WeaponBreak(true);

                break;
            case EventName.Damage:

                if (buff.IsReset)
                    Damage(true);

                break;

            case EventName.DamageUp:

                if (buff.IsReset)
                    DamageUp(true);

                break;
            case EventName.DefenseUp:

                if (buff.IsReset)
                    DefenseUp(true);

                break;
            case EventName.StormGust:

                if (buff.IsReset)
                    StormGust(true);

                break;

            case EventName.MeteorStrike:
            case EventName.Heal:
            case EventName.ReStart_Teleport:
            case EventName.Reverse:
            case EventName.Slow:
            case EventName.SpeedBoost:
                break;
        }
    }

    private void AmorBreak(bool isTurnOver)
    {
        if (!isTurnOver)
        {
            self.Data.Info.SetDef(-Model.Def, true);

            self.playerBuffAnimation.Callback = GamePlayController.Instance.CurrentPlayer.EndTurn;
            self.playerBuffAnimation.Active(ActiveBuff.Def);
        }
        else
        {
            self.Data.Info.SetDef(-Model.Def, true);
        }
    }

    private void WeaponBreak(bool isTurnOver)
    {
        if (!isTurnOver)
        {
            self.Data.Info.SetAtk(-Model.Atk, true);
            self.Hud.SetAtk(self.Data.Info.Status.Atk);

            self.playerBuffAnimation.Callback = GamePlayController.Instance.CurrentPlayer.EndTurn;
            self.playerBuffAnimation.Active(ActiveBuff.Atk);
        }
        else
        {
            self.Data.Info.SetAtk(Model.Atk, true);
            self.Hud.SetAtk(self.Data.Info.Status.Atk);
        }
    }

    private void Damage(bool isTurnOver)
    {
        if (!isTurnOver)
        {
            self.Data.Info.SetHp(-Model.Hp, true);
            self.Hud.SetHp(self.Data.Info.Status.Hp);

            self.playerBuffAnimation.Callback = GamePlayController.Instance.CurrentPlayer.EndTurn;
            self.playerBuffAnimation.Active(ActiveBuff.Hp);
        }
        else
        {
            self.Data.Info.SetHp(Model.Hp, true);
            self.Hud.SetHp(self.Data.Info.Status.Hp);
        }
    }

    private void MeteorStrike()
    {
        foreach (var block in Model.BlockIndexes)
        {
            var key = block.Key;
            var value = block.Value;

            if (value > -1)
            {
                var player = GamePlayController.Instance.players.Find(x => x.Data.Profile.Id == value);

                player.Data.Info.SetHp(-Model.Hp, true);
                player.Hud.SetHp(self.Data.Info.Status.Hp);
            }
        }

        GamePlayController.Instance.CurrentPlayer.EndTurn();
    }

    private void StormGust(bool isTurnOver)
    {

    }

    private void MonsterInvades(int turn, int survival)
    {

    }

    private void Heal()
    {
        self.Data.Info.SetHp(Model.Hp, true);
        self.Hud.SetHp(self.Data.Info.Status.Hp);

        self.playerBuffAnimation.Callback = GamePlayController.Instance.CurrentPlayer.EndTurn;
        self.playerBuffAnimation.Active(ActiveBuff.Atk);
    }

    private void DamageUp(bool isTurnOver)
    {
        if (!isTurnOver)
        {
            self.Data.Info.SetAtk(Model.Atk, true);
            self.Hud.SetAtk(self.Data.Info.Status.Atk);

            self.playerBuffAnimation.Callback = GamePlayController.Instance.CurrentPlayer.EndTurn;
            self.playerBuffAnimation.Active(ActiveBuff.Atk);
        }
        else
        {
            self.Data.Info.SetAtk(-Model.Atk, true);
            self.Hud.SetAtk(self.Data.Info.Status.Atk);
        }
    }

    private void DefenseUp(bool isTurnOver)
    {
        if (!isTurnOver)
        {
            self.Data.Info.SetDef(Model.Def, true);

            self.playerBuffAnimation.Callback = GamePlayController.Instance.CurrentPlayer.EndTurn;
            self.playerBuffAnimation.Active(ActiveBuff.Atk);
        }
        else
        {
            self.Data.Info.SetDef(-Model.Def, true);
        }
    }

    private void Restart()
    {
        self.playerBuffAnimation.Callback = self.playerMovment.Home;
        self.playerBuffAnimation.Active(ActiveBuff.Teleport);
    }

    private void Reverse()
    {

    }

    private void Slow()
    {

    }

    private void SpeedBoost()
    {

    }
}

public class EventEffectStateModel
{
    public double Hp { get; set; }
    public double Atk { get; set; }
    public double Def { get; set; }
    public int BlockIndex { get; set; }
    public Dictionary<int, int> BlockIndexes = new Dictionary<int, int>();
    public DiceFace DiceFace { get; set; }

    // Boss
    public bool IsLowBossBorn { get; set; }
    public bool IsMiddleBossBorn { get; set; }
    public bool IsTopBossBorn { get; set; }
    public bool IsMvpBossBorn { get; set; }
}