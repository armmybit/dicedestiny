﻿using System;

[Serializable]
public class ShopEffectState : IBuff
{
    private const string TAG = "ShopEffectState";

    public ShopEffectStateModel Model { get; set; }

    protected void Set(ShopEffectStateModel effect)
    {
        Model = effect;
        Effect();

        self.Data.Info.SetCurrency(-buff.Price, true);
        self.Hud.SetCurrency(self.Data.Info.Status.Currency);
    }

    protected void Update()
    {
        Effect();
    }

    private void Effect()
    {
        ShopName name = (ShopName)buff.Id;

        switch (name)
        {
            case ShopName.TheDice:
                TheDice();
                break;
            case ShopName.RedPotion:
                RedPotion();
                break;
            case ShopName.Wing:
                Wing();
                break;
            case ShopName.TomeOfKnowledge:
                TomeOfKnowledge();
                break;
            case ShopName.GoddessTear:
                GoddessTear();
                break;
            case ShopName.AttackPotion:
                AttackPotion(false);
                break;
            case ShopName.DefPotion:
                DefensePotion(false);
                break;
        }
    }

    protected void Restore(int id)
    {
        ShopName name = (ShopName)id;

        switch (name)
        {
            case ShopName.TheDice:
            case ShopName.RedPotion:
            case ShopName.Wing:
            case ShopName.TomeOfKnowledge:
            case ShopName.GoddessTear:
                break;

            case ShopName.AttackPotion:
                if (buff.IsReset)
                    AttackPotion(true);
                break;
            case ShopName.DefPotion:
                if (buff.IsReset)
                    DefensePotion(true);
                break;
        }
    }

    private void TheDice()
    {

    }

    private void RedPotion()
    {
        JulietLogger.Info(TAG, "RedPotion " + Model.Hp);

        self.Data.Info.SetHp(Model.Hp, true);
        self.Hud.SetHp(self.Data.Info.Status.Hp);

        self.playerBuffAnimation.Callback = GamePlayController.Instance.CurrentPlayer.EndTurn;
        self.playerBuffAnimation.Active(ActiveBuff.Hp);
    }

    private void Wing()
    {
        self.playerBuffAnimation.Callback = self.playerMovment.Teleport;
        self.playerBuffAnimation.Active(ActiveBuff.Teleport);
    }

    private void TomeOfKnowledge()
    {

    }

    private void GoddessTear()
    {
        JulietLogger.Info(TAG, "GoddessTear Hp " + Model.Hp + ", isHeal " + Model.IsHeal);

        self.Data.Info.SetHp(Model.Hp, true);
        self.Hud.SetHp(self.Data.Info.Status.Hp);

        self.playerBuffAnimation.Callback = GamePlayController.Instance.CurrentPlayer.EndTurn;
        self.playerBuffAnimation.Active(ActiveBuff.Hp);
    }

    private void AttackPotion(bool isTimeOver)
    {
        JulietLogger.Info(TAG, "AttackPotion Atk " + Model.Atk);

        if (!isTimeOver)
        {
            self.Data.Info.SetAtk(Model.Atk, true);
            self.Hud.SetAtk(self.Data.Info.Status.Atk);

            self.playerBuffAnimation.Callback = GamePlayController.Instance.CurrentPlayer.EndTurn;
            self.playerBuffAnimation.Active(ActiveBuff.Atk);
        }
        else
        {
            self.Data.Info.SetAtk(-Model.Atk, true);
            self.Hud.SetAtk(self.Data.Info.Status.Atk);
        }
    }

    private void DefensePotion(bool isTimeOver)
    {
        JulietLogger.Info(TAG, "DefensePotion Def " + Model.Def);

        if (!isTimeOver)
        {
            self.Data.Info.SetDef(Model.Def, true);

            self.playerBuffAnimation.Callback = GamePlayController.Instance.CurrentPlayer.EndTurn;
            self.playerBuffAnimation.Active(ActiveBuff.Def);
        }
        else
        {
            self.Data.Info.SetDef(-Model.Def, true);
        }
    }
}

public class ShopEffectStateModel
{
    public bool IsContinueRoll { get; set; }
    public double Hp { get; set; }
    public int BlockIndex { get; set; }
    public int LevelUp { get; set; }
    public bool IsHeal { get; set; }
    public double Atk { get; set; }
    public double Def { get; set; }
}