﻿
public class Shop : ShopEffectState
{
    public override void Apply(BuffInfoModel b, ModelPlayer p, BuffState s)
    {
        base.Apply(b, p, s);
    }

    public override void Use(ShopEffectStateModel effect)
    {
        Set(effect);
    }

    public override void Update(BuffState updateState)
    {
        base.Update(updateState);

        Update();
    }

    public override void Restore(BuffState restreState)
    {
        base.Restore(restreState);

        Restore(buff.Id);
    }
}