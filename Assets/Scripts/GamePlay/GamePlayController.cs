﻿using FullInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayController : BaseBehavior<JsonNetSerializer>, IGame
{
    #region singleton

    private static GamePlayController _instance;

    public static GamePlayController Instance
    {
        get
        {
            return _instance;
        }
    }

    public static GamePlayController GetInstance()
    {
        return _instance;
    }

    protected override void Awake()
    {
        base.Awake();

        _instance = this;
    }

    #endregion

    private const string TAG = "GamePlayController";

    [Header("Status")]
    public GameState state = GameState.None;

    [Header("Data")]
    public List<ModelPlayer> players = new List<ModelPlayer>();
    public List<ModelMonster> monsters = new List<ModelMonster>();
    public List<RenderTexture> facePlayers = new List<RenderTexture>();
    public ModelPath path;
    public ModelDices dice;

    [Header("Prefab")]
    public GameObject prefabPlayerModel;
    public GameObject prefabPlayerHud;
    public GameObject prefabOrders;
    public GameObject prefabPlayerCard;
    public GameObject prefabMonsterModel;
    public GameObject prefabTarget;

    [Header("Panel - Container")]
    public PanelPlayer panelPlayer;
    public PanelShop panelShop;
    public PanelEvent panelEvent;
    public PanelCard panelCard;
    public PanelRoll panelRoll;
    public PanelRoulette panelRoulette;
    public PanelOrder panelOrder;
    public PanelChooseOrder panelChooseOrder;
    public PanelTurn panelTurn;
    public PanelTarget panelTarget;
    public PanelResult panelResult;

    [Header("Parent")]
    public GameObject containerPlayers;
    public GameObject containerOrders;
    public GameObject containerCard;
    public GameObject containerTarget;

    // Player
    public ModelPlayer CurrentPlayer { get; set; }

    #region  Internal Method

    private void Start()
    {
        ChangeGameState(GameState.None);
        PlayerConnectedToGamePlay();
    }

    private void ChangeGameState(GameState state)
    {
        this.state = state;

        switch (state)
        {
            case GameState.None:
                DefaultState();
                break;
            case GameState.ChooseOrder:
                ChooseOrderState();
                break;
            case GameState.ShowOrder:
                ShowOrderState();
                break;
            case GameState.Prepare:
                PrepareState();
                break;
            case GameState.DrawCard:
                DrawCardState();
                break;
            case GameState.Start:
                StartState();
                break;
            case GameState.End:
                DefaultState();
                break;
        }
    }

    private void DefaultState()
    {
        this.panelPlayer.Toggle(false);
        this.panelShop.Toggle(false);
        this.panelEvent.Toggle(false);
        this.panelCard.Toggle(false);
        this.panelRoll.Toggle(false);
        this.panelRoulette.Toggle(false);
        this.panelOrder.Toggle(false);
        this.panelChooseOrder.Toggle(false);
        this.panelTurn.Toggle(false);
        this.panelTarget.Toggle(false);
        this.panelResult.Toggle(false);
    }

    private void ChooseOrderState()
    {
        this.panelChooseOrder.Toggle(true);
    }

    private void ShowOrderState()
    {
        this.panelOrder.Toggle(true);
        this.panelChooseOrder.Toggle(false);
    }

    private void PrepareState()
    {
        this.panelPlayer.Toggle(true);
        this.panelOrder.Toggle(false);
        this.panelChooseOrder.Toggle(false);
    }

    private void DrawCardState()
    {
        this.panelPlayer.Toggle(true);
        this.panelChooseOrder.Toggle(false);
    }

    private void StartState()
    {
        CameraController.Instance.Init();
    }

    private ModelPlayer CreatePlayerModel
    {
        get
        {
            var obj = Util.AddChild(null, this.prefabPlayerModel);

            return obj.GetComponent<ModelPlayer>();
        }
    }

    private PlayerHud CreatePlayerHud
    {
        get
        {
            var obj = Util.AddChild(this.containerPlayers, this.prefabPlayerHud);

            return obj.GetComponent<PlayerHud>();
        }
    }

    private RenderTexture GetPlayerFace
    {
        get
        {
            return this.facePlayers[this.players.Count];
        }
    }

    private PlayerOrder CreatePlayerOrder
    {
        get
        {
            var obj = Util.AddChild(this.containerOrders, this.prefabOrders);

            return obj.GetComponent<PlayerOrder>();
        }
    }

    private PlayerOwnCard CreateCard
    {
        get
        {
            var obj = Util.AddChild(this.containerCard, this.prefabPlayerCard);

            return obj.GetComponent<PlayerOwnCard>();
        }
    }

    private Target CreateTarget
    {
        get
        {
            var obj = Util.AddChild(this.containerTarget, this.prefabTarget);

            return obj.GetComponent<Target>();
        }
    }

    private ModelMonster CreateMonsterModel
    {
        get
        {
            var obj = Util.AddChild(null, this.prefabMonsterModel);

            return obj.GetComponent<ModelMonster>();
        }
    }

    #endregion

    #region Implement Method

    public void PlayerConnectedToGamePlay()
    {
        var req = new PlayerConnectedRequest()
        {
            RoomId = RoomSetting.RoomId,
            PlayerId = User.Profile.Id
        };

        ServerHub.PlayerConnected(req);
    }

    public void OnPlayerConnectedToGamePlay(PlayerConnectedResponse res)
    {
        JulietLogger.Info(TAG, "Waiting for other players");
    }

    public void OnCompletedPlayersConnected(ConnectedPlayersResponse res)
    {
        ChangeGameState(res.GameState);
    }

    public void PlayerChooseOrder(int index)
    {
        var req = new ChooseOrderRequest()
        {
            RoomId = RoomSetting.RoomId,
            Id = User.Profile.Id,
            Index = index
        };

        ServerHub.PlayerChooseOrder(req);
    }

    public void OnPlayerChooseOrder(ChooseOrderResponse res)
    {
        //ChangeGameState(res.GameState);

        // Show animation order
        this.panelChooseOrder.SetPlayerOrderList(res.Numbers);

        // After show animation order
        // StartCoroutine(DisplayOrder(res));
    }

    public IEnumerator DisplayOrder(ChooseOrderResponse res)
    {
        yield return new WaitForSeconds(3f);

        int i = 0;
        int length = RoomSetting.Players.Count;

        for (; i < length; i++)
        {
            // For Order
            var player = RoomSetting.Players[res.Numbers[i] - 1];
            var realOrder = i + 1;

            // Set Player Order
            var playerOrder = CreatePlayerOrder;
            playerOrder.SetContent(player.Profile.Name, realOrder);
        }
    }

    public void PrepareGame(PrepareGameResponse res)
    {
        CreatePath(res.Blocks);
        CreatePlayer(res.Orders);
        ChangeGameState(res.GameState);
    }

    public void CreatePath(List<Block> blocksInfo)
    {
        this.path.SetContent(blocksInfo);
    }

    public void CreatePlayer(List<int> orders)
    {
        int i = 0;
        int length = RoomSetting.Players.Count;

        for (; i < length; i++)
        {
            // For Detail
            var id = orders[i];
            var playerByOrder = RoomSetting.Players.Find(x => x.Profile.Id == id);
            var order = i + 1;

            // Get Player Basement
            var info = this.path.blocks.Find(x => x.Info.OwnerBlock == id).Info;

            // Set Player Hud and Model detail
            var model = CreatePlayerModel;
            var hud = CreatePlayerHud;
            var face = GetPlayerFace;
            var layerId = 8 + this.players.Count;

            model.SetInfo(playerByOrder, true, order);
            model.SetHud(hud, face);
            model.SetCamera(face, layerId);
            model.SetBasement(info);
            model.SetPosition(this.path.blocks);

            this.players.Add(model);
        }
    }

    public void CreateMonster(Monster monster, Block blockUpdate)
    {
        this.path.UpdateInfo(blockUpdate);

        var model = CreateMonsterModel;
        model.SetInfo(monster);
        model.SetPosition(this.path.blocks, blockUpdate);

        this.monsters.Add(model);
    }

    public void PlayerDrawCard(PlayerDrawCard card)
    {

    }

    public void StartGame(StartGameResponse res)
    {
        ChangeGameState(res.GameState);
        PlayerTurn(res.Id, res.Round);
    }

    public void PlayerTurn(int id, int round, Dictionary<BuffType, List<PlayerBuffOnGoing>> buffs = null)
    {
        CurrentPlayer = this.players.Find(x => x.Data.Profile.Id == id);
        CurrentPlayer.SetBuff(buffs);
        CurrentPlayer.Data.Info.State = PlayerState.Start;
        CurrentPlayer.StartTurn();

        CameraController.Instance.ChangePlayer(this.players.IndexOf(CurrentPlayer));

        // if (CurrentPlayer.Data.Info.Status.IsBot)
        // {
        //     panelRoll.Toggle(false);
        //     panelCard.Toggle(false);
        // }

        this.panelTurn.SetContent(round, CurrentPlayer.Data.Profile.Name);
        this.panelPlayer.Toggle(true);
        this.panelCard.Toggle(true);
        this.panelRoll.Toggle(true);
        this.panelTurn.Toggle(true);
    }

    public void Roll(GameRoll roll)
    {
        CurrentPlayer.Hud.countDown.Pause();

        var req = new RollRequest()
        {
            RoomId = RoomSetting.RoomId,
            Id = CurrentPlayer.Data.Profile.Id,
            Roll = roll
        };

        ServerHub.Roll(req);
    }

    public void OnRoll(RollResponse res)
    {
        // if (!CurrentPlayer.Data.Info.Status.IsBot)
        // {
        //     panelRoll.Toggle(false);
        //     dice.Open(res.DiceFace);
        // }

        //res.DiceFace.Number1 = 1;
        //res.DiceFace.Number2 = 1;

        this.panelRoll.Toggle(false);
        this.dice.Open(res.DiceFace);
        CurrentPlayer.Move(res.DiceFace);
    }

    public void PlayerArrived(Block info, BlockCheckState state = BlockCheckState.DetectOpponent)
    {
        PlayerArrivedRequest req = new PlayerArrivedRequest()
        {
            State = state,
            Id = CurrentPlayer.Data.Profile.Id,
            RoomId = RoomSetting.RoomId,
            Info = info
        };

        ServerHub.PlayerArrived(req);
    }

    public void OnDetectOpponent(DetectOpponentResponse res)
    {
        // Update Block
        CurrentPlayer.UpdateBlock(res.BlockUpdate);
        this.path.UpdateInfo(res.BlockUpdate);

        // Set new data
        CurrentPlayer.Data.Info.SetHp(res.Opponent.AttkerHp);
        var defPlayer = this.players.Find(x => x.Data.Profile.Id == res.Opponent.DefenderId);
        defPlayer.Data.Info.SetHp(res.Opponent.DefenderHp);

        // Start Battle
        BattlePlayer(defPlayer, BlockCheckState.DetectBlockType);
    }

    public void BattlePlayer(ModelPlayer opponent, BlockCheckState state = BlockCheckState.End)
    {
        BattleController.Instance.Short(CurrentPlayer.battleMovement, opponent.battleMovement, state);
    }

    public void BattlePlayer(ModelMonster opponent, BlockCheckState state = BlockCheckState.End)
    {
        BattleController.Instance.Short(CurrentPlayer.battleMovement, opponent.battleMovement, state);
    }

    public void BattleMonster(int opponent)
    {
        var monster = this.monsters.Find(x => x.Data.Profile.Id == opponent);

        BattleController.Instance.Short(CurrentPlayer.battleMovement, monster.battleMovement, BlockCheckState.End);
    }

    //public void BattleRemoteAttack(ModelPlayer opponent)
    //{
    //    BattleController.Instance.Remote(CurrentPlayer.battleMovement, opponent.battleMovement, BlockCheckState.End);
    //}

    //public void BattleRemoteAttack(ModelMonster opponent)
    //{
    //    BattleController.Instance.Remote(CurrentPlayer.battleMovement, opponent.battleMovement, BlockCheckState.End);
    //}

    public void OnBattleEnd(BlockCheckState state)
    {
        switch (state)
        {
            case BlockCheckState.DetectBlockType:
                PlayerArrived(CurrentPlayer.Data.Info.BreakPoints, BlockCheckState.DetectBlockType);
                break;
            case BlockCheckState.End:
                CurrentPlayer.EndTurn();
                break;
        }
    }

    public void OnDetectBlock(DetectBlockResponse res)
    {
        switch (res.BlockType)
        {
            case BlockType.None:
                CurrentPlayer.EndTurn();
                break;

            case BlockType.Castle:

                CurrentPlayer.UpdateBasement(res.BaseLevel, res.Currency);

                var block = this.path.blocks.Find(x => x.Info.OwnerBlock == CurrentPlayer.Data.Profile.Id);
                block.LevelIncrease(res.BaseLevel);

                break;

            case BlockType.Flag:

                this.panelTarget.Clear(this.containerTarget);

                res.Targets.ForEach(x =>
                {
                    var target = CreateTarget;
                    string targetName = this.players.Find(y => y.Data.Profile.Id == x).Data.Profile.Name;
                    target.SetContent(x, targetName);

                    this.panelTarget.SetTarget(target);
                });

                this.panelTarget.Toggle(true);

                break;

            case BlockType.Monster:

                if (res.IsCreateNew)
                    CreateMonster(res.Monster, res.BlockUpdate);

                BattleMonster(res.Monster.Profile.Id);

                break;

            case BlockType.Event:

                this.panelEvent.Toggle(true);
                this.panelEvent.SetEvent(res.EventItem);

                break;

            case BlockType.Shop:

                this.panelShop.Toggle(true);
                this.panelShop.SetShop(res.ShopsItem);

                break;

            case BlockType.Treasure:

                break;
        }
    }

    public void Attack(CharacterType characterType, int id)
    {
        this.panelTarget.Toggle(false);

        var req = new AttackRequest()
        {
            RoomId = RoomSetting.RoomId,
            AttackerId = CurrentPlayer.Data.Profile.Id,
            AttackerType = CharacterType.Player,
            DefenderId = id,
            DefenderType = characterType
        };

        ServerHub.Attack(req);
    }

    public void OnPlayerAttack(AttackResponse res)
    {
        CurrentPlayer.Data.Info.SetHp(res.AttkerHp);

        switch (res.DefenderType)
        {
            case CharacterType.Player:

                var defPlayer = this.players.Find(x => x.Data.Profile.Id == res.DefenderId);
                defPlayer.Data.Info.SetHp(res.DefenderHp);
                BattlePlayer(defPlayer);

                break;
            case CharacterType.Monster:

                var defMonster = this.monsters.Find(x => x.Data.Profile.Id == res.DefenderId);
                defMonster.Data.Info.SetHp(res.DefenderHp);
                BattlePlayer(defMonster);

                break;
        }
    }

    public void BuyItem(bool isBuy, int shopId = -1)
    {
        this.panelShop.Toggle(false);

        var req = new BuyItemRequest()
        {
            Id = CurrentPlayer.Data.Profile.Id,
            RoomId = RoomSetting.RoomId,
            IsBuy = isBuy,
            ShopId = shopId
        };

        ServerHub.BuyItem(req);
    }

    public void OnBuyItem(BuyItemResponse res)
    {
        if (res.IsCancel)
        {
            CurrentPlayer.EndTurn();
        }
        else
        {
            CurrentPlayer.SetBuff(res);
        }
    }

    public void ReceiveEvent(int eventId)
    {
        var req = new ReceiveEventRequest()
        {
            EventId = eventId,
            Id = CurrentPlayer.Data.Profile.Id,
            RoomId = RoomSetting.RoomId
        };

        ServerHub.ReceiveEvent(req);
    }

    public void OnReceiveEvent(ReceiveEventResponse res)
    {
        CurrentPlayer.SetBuff(res);
    }

    public void EndTurn()
    {
        var req = new EndTurnRequest()
        {
            Id = CurrentPlayer.Data.Profile.Id,
            RoomId = RoomSetting.RoomId
        };

        ServerHub.EndTurn(req);
    }

    public void OnEndTurn(EndTurnResponse res)
    {
        PlayerTurn(res.Id, res.Round, res.Buffs);

        if (res.Round > 1)
            PlayerDrawCard(res.Card);
    }

    public void UseCard(int cardId, int playerTarget)
    {
        var req = new UseCardRequest()
        {
            RoomId = RoomSetting.RoomId,
            PlayerId = CurrentPlayer.Data.Profile.Id,
            CardId = cardId,
            TargetId = playerTarget
        };

        ServerHub.UseCard(req);
    }

    public void OnUseCard()
    {

    }

    public void OnEndGame(EndGameResponse res)
    {
        // Winner
        var winner = this.players.Find(x => x.Data.Profile.Id == res.Winner);

        // Looser
        var loosers = this.players.FindAll(x => x.Data.Profile.Id != res.Winner);

        this.panelResult.Toggle(true);
        this.panelResult.SetWinner(winner);
        this.panelResult.SetLoosers(loosers);
    }

    #endregion
}