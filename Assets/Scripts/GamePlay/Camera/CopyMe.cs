﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CopyMe : MonoBehaviour
{
    public Text texty;
    public Transform objParent;
    public GameObject obj;

    int count = 1;



    public void Copy()
    {
        GameObject g = Instantiate(obj, Vector3.up * 2, Quaternion.identity, objParent.transform);
        g.name = "Clone_CopyMe";
        count++;
        texty.text = count.ToString();
    }
}
