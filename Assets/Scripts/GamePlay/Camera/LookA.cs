﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookA : MonoBehaviour
{
	public Transform target;

	void Update ()
	{
		transform.LookAt (target.position);
		transform.RotateAround (Vector3.zero, Vector3.up, 20 * Time.deltaTime);
	}
}
