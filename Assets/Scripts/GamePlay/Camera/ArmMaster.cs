﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ArmMaster : MonoBehaviour
{

	public Transform targetPos;

	NavMeshAgent agent;

	//	public NavMeshPath navPath;

	// Use this for initialization
	void Start ()
	{
		agent = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.A)) {
//			agent.updatePosition = false;
//			agent.SetPath (navPath);
			agent.Warp (targetPos.position);
//			agent.SetDestination (targetPos.position);
			transform.position = targetPos.position;
		}

		if (Input.GetKeyDown (KeyCode.S)) {
//			agent.updatePosition = true;
//			transform.position = targetPos.position;
		}
	}
}
