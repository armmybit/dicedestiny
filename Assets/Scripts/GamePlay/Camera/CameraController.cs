﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    #region Singleton

    private static CameraController _instance;

    public static CameraController Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;

        //    // targetGroup = FindObjectOfType<CinemachineTargetGroup>();
        cameras = FindObjectsOfType<CameraHandler>().ToList();
        //    // ChangeCamera(GameCamera.Follow);

        for (int i = 0; i < cameras.Count; i++)
        {
            cameras[i].gameObject.SetActive(false);
        }
    }

    #endregion

    // Cameras and Group
    public List<CameraHandler> cameras;
    public CinemachineTargetGroup targetGroup;

    // Current Using
    public CameraHandler current;

    // Light
    public GameObject lightMain;
    public GameObject lightBattle;

    public Transform[] battlePositions;

    public void Init()
    {
        for (int i = 0; i < GamePlayController.Instance.players.Count; i++)
        {
            var cameraHandle = this.cameras[i];

            cameraHandle.Toggle(false);
            cameraHandle.Init();
            cameraHandle.virtualCamera.Follow = GamePlayController.Instance.players[i].transform;
            cameraHandle.virtualCamera.LookAt = GamePlayController.Instance.players[i].transform;
        }
    }

    public void ChangePlayer()
    {
        for (int i = 0; i < GamePlayController.Instance.players.Count; i++)
        {
            if (GamePlayController.Instance.players[i] == GamePlayController.Instance.CurrentPlayer)
            {
                cameras[i].gameObject.SetActive(true);
            }
            else
            {
                cameras[i].gameObject.SetActive(false);
            }
        }
    }

    public void ChangePlayer(int index)
    {
        if (this.current != null)
            this.current.Toggle(false);

        this.current = this.cameras[index];
        this.current.Toggle(true);
    }

    public IEnumerator Focus(float deltaTime, Transform focus, int index)
    {
        yield return new WaitForSeconds(deltaTime);

        if (index >= 0)
        {
            this.current.type = GameCamera.Follow;
            this.current.focus = null;
            this.current.virtualCamera.m_Lens.FieldOfView = 40;
            this.current.Toggle(false);

            this.current = this.cameras[index];
            this.current.type = GameCamera.BattleFocus;
            this.current.virtualCamera.m_Lens.FieldOfView = 35;
            this.current.Toggle(true);
        }
        else
        {
            this.current.type = GameCamera.BattleFocus;
            this.current.focus = focus;
            this.current.virtualCamera.m_Lens.FieldOfView = 35;
        }
    }

    public IEnumerator EndFocus(float deltaTime, int index)
    {
        yield return new WaitForSeconds(deltaTime);

        this.current.type = GameCamera.Follow;
        this.current.focus = null;
        this.current.virtualCamera.m_Lens.FieldOfView = 40;
        this.current.Toggle(false);

        this.current = this.cameras[index];
        this.current.type = GameCamera.Follow;
        this.current.virtualCamera.m_Lens.FieldOfView = 40;
        this.current.Toggle(true);
    }

    public void ChangeCamera(GameCamera type)
    {
        cameras.ForEach(x =>
        {
            if (x.type == type)
            {
                x.gameObject.SetActive(true);

                if (x.type == GameCamera.Follow)
                {
                    x.GetComponent<CinemachineVirtualCamera>().Follow = GamePlayController.Instance.CurrentPlayer.transform;
                    x.GetComponent<CinemachineVirtualCamera>().LookAt = GamePlayController.Instance.CurrentPlayer.transform;
                }
            }
            else
            {
                x.gameObject.SetActive(false);
            }
        });
    }
}
