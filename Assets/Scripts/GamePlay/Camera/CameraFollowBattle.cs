﻿using System.Collections;
using UnityEngine;

public class CameraFollowBattle : MonoBehaviour
{
    public Transform followTarget;
    public Transform lookTarget;

    private static CameraFollowBattle _instance;

    public static CameraFollowBattle Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }

    private void Update()
    {
        if (lookTarget != null)
        {
            Vector3 lookPos = lookTarget.position - transform.position;
            lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 10);

            transform.position = followTarget.position;
        }
    }

    public void SetLookTarget(Transform target)
    {
        //		lookTarget = target;
        StartCoroutine(Delay(target));
    }

    private IEnumerator Delay(Transform target)
    {
        yield return new WaitForSeconds(2f);
        lookTarget = target;
    }
}