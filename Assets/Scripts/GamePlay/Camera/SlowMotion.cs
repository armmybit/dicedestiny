﻿using UnityEngine;

public class SlowMotion : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.A))
        {
            Time.timeScale = 1;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Time.timeScale = .5f;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            Time.timeScale = .25f;
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            Time.timeScale = .1f;
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            BattleTransitionController.Instance.Play();
        }
    }
}
