﻿using UnityEngine;
using Cinemachine;

public class CameraHandler : MonoBehaviour
{
    public CinemachineVirtualCamera virtualCamera;
    public GameCamera type;
    public Transform focus;

    public void Init()
    {
        virtualCamera = GetComponent<CinemachineVirtualCamera>();
    }

    public void Toggle(bool isActive)
    {
        gameObject.SetActive(isActive);
    }

    void Update()
    {
        if (this.type == GameCamera.Follow && GamePlayController.Instance.CurrentPlayer != null)
        {
            this.virtualCamera.Follow = GamePlayController.Instance.CurrentPlayer.transform;
            this.virtualCamera.LookAt = GamePlayController.Instance.CurrentPlayer.transform;
        }
        else if (this.type == GameCamera.BattleFocus && this.focus != null)
        {
            this.virtualCamera.Follow = this.focus;
            this.virtualCamera.LookAt = this.focus;
        }
    }
}