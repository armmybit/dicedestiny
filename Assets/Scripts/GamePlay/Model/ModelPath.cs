﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using CI.TaskParallel;

public class ModelPath : MonoBehaviour
{
    public List<ModelBlock> blocks = new List<ModelBlock>();

    void Awake()
    {
        blocks = GetComponentsInChildren<ModelBlock>().ToList();
    }

    public void SetContent(List<Block> blocksInfo)
    {
        for (int i = 0; i < blocksInfo.Count; i++)
        {
            var block = blocks[i];
            var info = blocksInfo[i];

            block.Init(info);
        }
    }

    public void UpdateInfo(Block info)
    {
        foreach (var block in this.blocks)
        {
            if (info.Index == block.Info.Index)
            {
                block.Info = info;
                break;
            }
        }
    }
}