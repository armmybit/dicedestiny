﻿using System.Collections.Generic;
using UnityEngine;

public class ModelPlayer : Model
{
    private const string TAG = "ModelPlayer";
    public PlayerHud Hud { get; set; }
    public Player Data { get; set; }

    [Header("Data")]
    public Camera cameraFace;
    public PlayerMovement playerMovment;
    public BattleMovement battleMovement;
    public PlayerBuffAnimation playerBuffAnimation;

    private void OnEnable()
    {
        this.playerMovment = GetComponent<PlayerMovement>();
        this.battleMovement = GetComponent<BattleMovement>();
        this.playerBuffAnimation = GetComponentInChildren<PlayerBuffAnimation>();
    }

    public void SetInfo(PlayerJoin join, bool isReady, int order)
    {
        Data = new Player(join, isReady, order, CharacterType.Player);
        CharacterType = CharacterType.Player;
    }

    public void SetHud(PlayerHud hud, RenderTexture renderTexture)
    {
        Hud = hud;
        Hud.SetContent(Data.Profile, Data.Info.Status, renderTexture);
    }

    public void SetCamera(RenderTexture renderTexture, int layerId)
    {
        Util.SetLayerRecursively(gameObject, layerId);

        this.cameraFace.targetTexture = renderTexture;
        this.cameraFace.cullingMask = (int)Mathf.Pow(2, (layerId - 8)) * 256;
    }

    public void SetBasement(Block info)
    {
        Data.Info.SetBreakPoints(info);
    }

    public void SetPosition(List<ModelBlock> blocks)
    {
        this.playerMovment.SetWayPoints(blocks, this);
    }

    public void SetCard(List<BuffInfoModel> cards)
    {
        Data.Card.Add(cards);
    }

    // Turn
    public void StartTurn()
    {
        Hud.StartCountDown(EndRoll);
    }

    public void EndRoll(bool isTimeOver)
    {

    }

    public void EndTurn()
    {
        Hud.ActiveTurnUI(false);
        GamePlayController.Instance.EndTurn();
    }

    // Walk
    public void Move(DiceFace face)
    {
        this.playerMovment.Move((face.Number1 + face.Number2));
    }

    // Block
    public void UpdateBlock(Block info)
    {
        Data.Info.BreakPoints = info;
    }

    public void UpdateBasement(int level, double currency)
    {
        // Update Data
        Data.Info.SetCurrency(currency);
        Data.Basement.SetLevel(level);

        // Update Result to Hud
        Hud.SetCurrency(Data.Info.Status.Currency);
    }

    // Buff
    public void SetBuff(BuyItemResponse res)
    {
        Data.Buff.Apply(res, this);
    }

    public void SetBuff(ReceiveEventResponse res)
    {
        Data.Buff.Apply(res, this);
    }

    public void SetBuff(Dictionary<BuffType, List<PlayerBuffOnGoing>> buffs)
    {
        if (buffs != null && buffs.Count > 0)
            Data.Buff.Update(buffs);
    }

    // Get Attack
    public bool GetAttack()
    {
        bool isDie = Data.Info.IsDied;

        Hud.SetHp(Data.Info.Status.Hp);

        return isDie;
    }
}