﻿using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;

public class ModelMonster : Model
{
    private const string TAG = "ModelMonster";

    public PlayerHud Hud { get; set; }
    public Monster Data { get; set; }

    [Header("Data")]
    public Camera cameraFace;
    public MonsterMovement monsterMovement;
    public BattleMovement battleMovement;

    private void OnEnable()
    {
        this.monsterMovement = GetComponent<MonsterMovement>();
        this.battleMovement = GetComponent<BattleMovement>();
    }

    public void SetInfo(Monster monster)
    {
        Data = monster;
        CharacterType = CharacterType.Monster;
    }

    public void SetPosition(List<ModelBlock> blocks, Block currenStayBlock)
    {
        this.monsterMovement.SetWayPoints(blocks, this, currenStayBlock);
    }

    // Get Attack
    public bool GetAttack()
    {
        bool isDie = Data.Info.IsDied;

        //Hud.SetHp(Data.Info.Status.Hp);
        //Hud.ActiveDefeated(isDie);

        return isDie;
    }
}