﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterMovement : MonoBehaviour
{
    public PlayerAnimation anim;
    public NavMeshAgent agent;
    public ModelMonster modelMonster;
    public int currentStep = 0;
    public int remainingStep = 0;
    public bool isGoAhead = true;
    public bool isArrived = true;

    public List<Transform> wayPoints = new List<Transform>();

    // Player Point
    public Transform homePosition;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    public void Init(ModelMonster self, Transform t)
    {
        homePosition = t;
        modelMonster = self;

        gameObject.transform.localPosition = homePosition.position;
        currentStep = wayPoints.IndexOf(homePosition);
    }

    public void SetWayPoints(List<ModelBlock> blocks, ModelMonster self, Block block)
    {
        blocks.ForEach(x =>
        {
            Transform transform = x.gameObject.transform;
            this.wayPoints.Add(transform);

            if (block.Index == x.Info.Index)
                Init(self, transform);
        });
    }

    public void Move(int number)
    {
        StartCoroutine(GoAhead(number));
    }

    public IEnumerator GoAhead(int number)
    {
        yield return new WaitForSeconds(2f);

        remainingStep = number;
        currentStep = (currentStep) % (wayPoints.Count);
        agent.SetDestination(wayPoints[currentStep].position);

        isGoAhead = true;
        isArrived = false;
    }

    public void GoBack(int number)
    {
        remainingStep = number;
        currentStep--;

        if (currentStep < 0)
        {
            currentStep = wayPoints.Count;
        }

        agent.SetDestination(wayPoints[currentStep].position);

        isGoAhead = false;
        isArrived = false;
    }

    public void Teleport()
    {
        currentStep = UnityEngine.Random.Range(0, wayPoints.Count);
        transform.position = wayPoints[currentStep].position;
        agent.SetDestination(wayPoints[currentStep].position);

        isArrived = false;
    }

    private void Clear()
    {
        remainingStep = 0;
    }

    private void Update()
    {
        if (isArrived)
            return;

        Move();
        return;
    }

    private void Move()
    {
        if (remainingStep == 0 && agent.remainingDistance <= agent.stoppingDistance && !isArrived)
        {
            Arrived();
        }

        if (!agent.pathPending && agent.remainingDistance < agent.stoppingDistance)
        {
            if (isGoAhead)
                NextPoint();
            else
                PreviousPoint();
        }
    }

    private void NextPoint()
    {
        if (remainingStep > 0)
        {
            remainingStep--;
            currentStep = (currentStep + 1) % (wayPoints.Count);

            StepMoving();
        }
    }

    private void PreviousPoint()
    {
        if (remainingStep > 0)
        {
            remainingStep--;
            currentStep--;

            if (currentStep < 0)
            {
                currentStep = wayPoints.Count;
            }

            StepMoving();
        }
    }

    private void StepMoving()
    {
        if (remainingStep == 0)
            agent.autoBraking = true;

        agent.SetDestination(wayPoints[currentStep].position);
    }

    private void Arrived()
    {
        isArrived = true;
        Clear();
    }

    private void ContinueMove(int tmpRemainStep)
    {
        if (isGoAhead)
            GoAhead(tmpRemainStep);
        else
            GoBack(tmpRemainStep);
    }

    public ModelBlock GetCurrentBlock()
    {
        return wayPoints[currentStep].GetComponent<ModelBlock>();
    }
}
