﻿
public class MonsterInfo
{
    // State
    public PlayerState State { get; set; }
    public Block BreakPoints { get; set; }
    public MonsterStatusModel Status { get; set; }
    
    public MonsterInfo(MonsterStatusModel status)
    {
        Status = status;
    }

    public void SetBreakPoints(Block newBreakPoints)
    {
        BreakPoints = newBreakPoints;
    }

    public void SetHp(double hp)
    {
        Status.Hp += hp;

        if (Status.Hp < 0)
            Status.Hp = 0;
    }

    public void SetAtk(double atk)
    {
        Status.Atk += atk;

        if (Status.Atk < 0)
            Status.Atk = 0;
    }

    public bool IsDied
    {
        get
        {
            if (Status.Hp <= 0)
                return true;
            return false;
        }
    }
}