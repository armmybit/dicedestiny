﻿
public class Monster : MasterCharacter
{
    public MonsterProfile Profile { get; set; }
    public MonsterInfo Info { get; set; }
    
    public Monster(MonsterProfile profile, MonsterInfo info, CharacterType charType)
    {
        CharacterType = charType;
        Profile = profile;
        Info = info;
    }
}