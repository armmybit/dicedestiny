﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class BattleMovement : MonoBehaviour
{
    public NavMeshAgent agent;
    private PlayerAnimation anim;
    private PlayerBattleState playerBattleState;
    public Weapon weapon;
    public BattleMovement enemy;
    private float fixStoppingDistance = .15f;

    public Action<Model> AttackCallback;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<PlayerAnimation>();
        weapon = GetComponentInChildren<Weapon>();
    }

    private void Update()
    {
        if (enemy != null)
        {
            LookTarget();
        }
    }

    private void LookTarget()
    {
        Vector3 lookPos = enemy.transform.position - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 10);
    }

    public void WarpToPosition(Vector3 pos)
    {
        agent.Warp(pos);
    }

    public void SetEnemy(BattleMovement enemy)
    {
        this.enemy = enemy;
    }

    public void MoveTowardsAndAttack(BattleMovement target, System.Action<bool> callback)
    {
        playerBattleState = PlayerBattleState.MoveTowardsAndAttack;
        agent.SetDestination(target.transform.position);
        anim.callback = callback;
    }

    public void EndState()
    {
        playerBattleState = PlayerBattleState.None;
        agent.SetDestination(transform.position);
    }

    public void EndBattle()
    {
        ResetStoppingDistance();
        agent.isStopped = false;
        enemy = null;
    }

    private void Attack(Model model)
    {
        agent.isStopped = true;
        weapon.SetAttack();
        anim.MoveAttack();
        UpdateModel(model);
    }

    public void ResetStoppingDistance()
    {
        agent.stoppingDistance = fixStoppingDistance;
    }

    private void OnTriggerEnter(Collider obj)
    {
        if (obj.GetComponent<BattleMovement>())
        {
            if (playerBattleState == PlayerBattleState.MoveTowardsAndAttack)
            {
                Attack(obj.GetComponent<Model>());
            }
        }
    }

    private void UpdateModel(Model model)
    {
        switch (model.CharacterType)
        {
            case CharacterType.Monster:

                var monsterModel = (ModelMonster)model;
                bool isMonsterDie = monsterModel.GetAttack();

                if (isMonsterDie)
                    anim.Dead();

                break;

            case CharacterType.Player:

                var playerModel = (ModelPlayer)model;
                bool isPlayerDie = playerModel.GetAttack();

                if (isPlayerDie)
                    anim.Dead();

                break;
        }
    }

    public bool IsDuringAttack()
    {
        return anim.isDuringAttack;
    }

    // You get hit from monster or player
    public void TakeDamage()
    {
        anim.TakeDamage();
    }

}
