﻿
public class PlayerBackup
{
    // Current Player
    public bool IsAttackerDie { get; set; }
    public double AttkerHp { get; set; }

    public int DefenderId { get; set; }
    public bool IsDefenderDie { get; set; }
    public CharacterType DefenderType { get; set; }
    public double DefenderHp { get; set; }
}