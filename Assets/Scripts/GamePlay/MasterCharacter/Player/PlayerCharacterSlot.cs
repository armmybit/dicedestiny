﻿
public class PlayerCharacterSlot
{
    public Player Self { get; set; }
    public PlayerFormCharacterModel main;
    public PlayerFormCharacterModel sub1;
    public PlayerFormCharacterModel sub2;

    public PlayerCharacterSlot(PlayerJoin join)
    {
        this.main = join.Main;
        this.sub1 = join.Sub1;
        this.sub2 = join.Sub2;
    }

    public void Init(Player self)
    {
        Self = self;
    }
}