﻿
public class PlayerInfo
{
    // State
    public PlayerState State { get; set; }
    public Block BreakPoints { get; set; }
    public PlayerStatusModel Status { get; set; }

    // Data
    public bool IsReady { get; set; }
    public int Order { get; set; }

    public PlayerInfo(PlayerStatusModel status, bool isReady, int order)
    {
        Status = status;
        IsReady = isReady;
        Order = order;
    }

    public void SetBreakPoints(Block breakPoints)
    {
        BreakPoints = breakPoints;
    }

    public void SetCurrency(double currency, bool isManualUpdate = false)
    {
        if (isManualUpdate)
            Status.Currency += currency;
        else
            Status.Currency = currency;

        if (Status.Currency < 0)
            Status.Currency = 0;
    }

    public void SetHp(double hp, bool isManualUpdate = false)
    {
        if (isManualUpdate)
            Status.Hp += hp;
        else
            Status.Hp = hp;

        if (Status.Hp < 0)
            Status.Hp = 0;
    }

    public void SetAtk(double atk, bool isManualUpdate = false)
    {
        if (isManualUpdate)
            Status.Atk += atk;
        else
            Status.Atk = atk;

        if (Status.Atk < 0)
            Status.Atk = 0;
    }

    public void SetDef(double def, bool isManualUpdate = false)
    {
        if (isManualUpdate)
            Status.Def += def;
        else
            Status.Def = def;

        if (Status.Def < 0)
            Status.Def = 0;
    }

    public bool IsDied
    {
        get
        {
            if (Status.Hp <= 0)
                return true;
            return false;
        }
    }
}