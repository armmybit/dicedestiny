﻿
public class PlayerProfile
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Level { get; set; }

    public PlayerProfile(PlayerProfileModel player)
    {
        Id = player.Id;
        Name = player.Name;
        Level = player.Level;
    }
}