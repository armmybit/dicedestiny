﻿
public class PlayerBasement
{
    public Player Self { get; set; }
    public PlayerFormBasementModel Info { get; set; }
    public int level;

    public PlayerBasement(PlayerFormBasementModel basement)
    {
        Info = basement;
        this.level = 1;
    }

    public void Init(Player self)
    {
        Self = self;
    }

    public void SetLevel(int level)
    {
        this.level = level;
    }
}