﻿
public class Player : MasterCharacter
{
    public PlayerProfile Profile { get; set; }
    public PlayerInfo Info { get; set; }
    public PlayerBasement Basement { get; set; }
    public PlayerBuff Buff { get; set; }
    public PlayerCharacterSlot Character { get; set; }
    public PlayerCard Card { get; set; }

    public Player(PlayerJoin join, bool isReady, int order, CharacterType charType)
    {
        CharacterType = charType;
        Profile = new PlayerProfile(join.Profile);
        Info = new PlayerInfo(join.Status, isReady, order);
        Basement = new PlayerBasement(join.Basement);
        Buff = new PlayerBuff();
        Character = new PlayerCharacterSlot(join);
        Card = new PlayerCard();

        Basement.Init(this);
        Character.Init(this);
    }
}