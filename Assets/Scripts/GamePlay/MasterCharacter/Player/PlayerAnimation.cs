﻿using UnityEngine;
using UnityEngine.AI;

public class PlayerAnimation : MonoBehaviour
{
    public Animator animator;
    public NavMeshAgent agent;
    private BattleMovement battleMovement;

    private const string key_isRun = "IsRun";
    private const string key_isAttack = "IsAttack";
    private const string key_isMoveAttack = "IsMoveAttack";
    private const string key_isDamage = "IsDamage";
    private const string key_isDead = "IsDead";
    private const string key_isPrepare = "IsPrepare";

    private const string STATE_NONE = "STATE_NONE";
    private const string STATE_ATTACK = "STATE_ATTACK";
    private const string STATE_END_TURN = "STATE_END_TURN";

    public string lastAnimation;
    public bool isDuringAttack;
    public bool isDie;

    public System.Action<bool> callback;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        agent = GetComponentInParent<NavMeshAgent>();
        battleMovement = GetComponentInParent<BattleMovement>();
    }

    private void Update()
    {
        if (agent == null)
            return;

        if (agent.remainingDistance > agent.stoppingDistance)
        {
            animator.SetBool(key_isRun, true);
        }
        else
        {
            animator.SetBool(key_isRun, false);
        }

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Idle"))
        {
            if (lastAnimation != "")
            {
                if (lastAnimation == key_isMoveAttack)
                {
                    isDuringAttack = false;
                    battleMovement.weapon.ResetAttack();
                    agent.stoppingDistance = agent.remainingDistance;
                    callback(isDie);
                }

                lastAnimation = "";
            }
        }


        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.RollBackward"))
        {
            agent.stoppingDistance = agent.remainingDistance;
        }

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Attack"))
        {
            if (animator.GetBool(key_isMoveAttack))
                animator.SetBool(key_isMoveAttack, false);

            if (lastAnimation != key_isMoveAttack)
                lastAnimation = key_isMoveAttack;

            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.4f)
                isDuringAttack = true;
        }

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Damage"))
        {
            animator.SetBool(key_isDamage, false);

            agent.stoppingDistance = agent.remainingDistance;
        }

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Dead"))
        {
            animator.SetBool(key_isDead, false);
        }
    }

    public void Attack()
    {
        animator.SetBool(key_isAttack, true);
    }

    public void MoveAttack()
    {
        animator.SetBool(key_isMoveAttack, true);
    }

    public void TakeDamage()
    {
        animator.SetBool(key_isDamage, true);
    }

    public void Dead()
    {
        Debug.Log("Dead!");
        animator.SetBool(key_isDead, true);
        this.isDie = true;
    }

    public void Teleport()
    {

    }

    void OnAnimatorMove()
    {
        if (lastAnimation == key_isMoveAttack)
            battleMovement.transform.position = animator.rootPosition;
    }
}
