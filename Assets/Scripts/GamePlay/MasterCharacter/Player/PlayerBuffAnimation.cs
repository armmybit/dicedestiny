﻿using System;
using System.Collections;
using UnityEngine;

public enum ActiveBuff
{
    None,
    Hp,
    Atk,
    Def,
    Teleport
}

public class PlayerBuffAnimation : MonoBehaviour
{
    public GameObject hpBuffObj;
    public GameObject atkBuffObj;
    public GameObject defBuffObj;
    public GameObject teleportBuffObj;

    public Action Callback { get; set; }
    public ActiveBuff Status { get; set; }

    public void Active(ActiveBuff a)
    {
        Status = a;

        switch (Status)
        {
            case ActiveBuff.Hp:
                this.hpBuffObj.SetActive(true);
                break;
            case ActiveBuff.Atk:
                this.atkBuffObj.SetActive(true);
                break;
            case ActiveBuff.Def:
                this.defBuffObj.SetActive(true);
                break;
            case ActiveBuff.Teleport:
                this.teleportBuffObj.SetActive(true);
                break;
        }

        StartCoroutine(End());
    }

    public IEnumerator End()
    {
        yield return new WaitForSeconds(2f);

        Callback?.Invoke();

        switch (Status)
        {
            case ActiveBuff.Hp:
                this.hpBuffObj.SetActive(false);
                break;
            case ActiveBuff.Atk:
                this.atkBuffObj.SetActive(false);
                break;
            case ActiveBuff.Def:
                this.defBuffObj.SetActive(false);
                break;
            case ActiveBuff.Teleport:
                this.teleportBuffObj.SetActive(false);
                break;
        }

        Callback = null;
        Status = ActiveBuff.None;
    }
}
