﻿
using System.Collections.Generic;

public class PlayerCard
{
    public List<BuffInfoModel> cards = new List<BuffInfoModel>();

    public void Add(List<BuffInfoModel> cards)
    {
        this.cards.AddRange(cards);
    }

    public BuffInfoModel Active(int id)
    {
        var card = this.cards.Find(x => x.Id == id);
        this.cards.Remove(card);

        return card;
    }

    public BuffInfoModel Trigger()
    {
        if (this.cards.Count > 0)
        {
            var card = this.cards.Find(x => x.TypeCard == CardType.Trigger);

            if (card != null)
                this.cards.Remove(card);

            return card;
        }

        return null;
    }
}