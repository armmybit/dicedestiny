﻿
using System.Collections.Generic;

public class PlayerBuff
{
    public List<IBuff> buffs = new List<IBuff>();

    // Shop
    public void Apply(BuyItemResponse res, ModelPlayer self) 
    {
        bool isHas = this.buffs.Exists(x => x.buff.Id == res.Buff.Id && x.buff.TypeBuff == BuffType.Shop);
        IBuff iBuff = new Shop();

        if (isHas)
            iBuff = this.buffs.Find(x => x.buff.Id == res.Buff.Id && x.buff.TypeBuff == BuffType.Shop);
        else
            iBuff.Apply(res.Buff, self, res.State);

        iBuff.Use(res.Effect);

        if (!res.Buff.IsImmediateDestroy)
            this.buffs.Add(iBuff);
    }

    // Event
    public void Apply(ReceiveEventResponse res, ModelPlayer self)
    {
        bool isHas = this.buffs.Exists(x => x.buff.Id == res.Buff.Id && x.buff.TypeBuff == BuffType.Event);
        IBuff iBuff = new Events();

        if (isHas)
            iBuff = this.buffs.Find(x => x.buff.Id == res.Buff.Id && x.buff.TypeBuff == BuffType.Event);
        else
            iBuff.Apply(res.Buff, self, res.State);

        iBuff.Use(res.Effect);

        if (!res.Buff.IsImmediateDestroy)
            this.buffs.Add(iBuff);
    }

    public void Update(Dictionary<BuffType, List<PlayerBuffOnGoing>> buffsOnGoing)
    {
        foreach (var buff in buffsOnGoing)
        {
            var ควย = buff.Value;

            ควย.ForEach(x =>
            {
                var iBuff = this.buffs.Find(y => y.buff.Id == x.Id && y.buff.TypeBuff == buff.Key);

                if (iBuff != null)
                {
                    switch (x.State)
                    {
                        case BuffState.Ready:
                            iBuff.Update(x.State);
                            break;
                        case BuffState.Expired:
                            iBuff.Restore(x.State);
                            buffs.Remove(iBuff);
                            break;
                    }
                }
            });
        }
    }
}