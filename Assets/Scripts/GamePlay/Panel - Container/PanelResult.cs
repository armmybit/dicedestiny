﻿using System.Collections.Generic;

public class PanelResult : Panel
{
    public PlayerResult winner;
    public List<PlayerResult> loosers;

    public void SetWinner(ModelPlayer winner)
    {
        this.winner.Open(1, winner.Data.Profile.Name, 36.40);
    }

    public void SetLoosers(List<ModelPlayer> loosersPlayer)
    {
        int i = 0;

        loosersPlayer.ForEach(x =>
        {
            var result = this.loosers[i];
            result.Open((i + 2), x.Data.Profile.Name, (20.00 + (i * 2)));
            i++;
        });
    }
}