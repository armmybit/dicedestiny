﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OrderCard : MonoBehaviour
{
    public RectTransform frontCardTransform;
    public RectTransform backCardTransform;
    public RectTransform numberCardTransform;

    public bool isClick;

    System.Action Callback;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnSelect);
    }

    public void OnClick_Choose()
    {
        // GamePlayController.Instance.PlayerChooseOrder(0);
    }


    void OnSelect()
    {
        if (isClick)
            return;

        isClick = true;

        GetComponentInParent<PanelChooseOrder>().OnSelectCard(this);
    }

    void RandomNumber()
    {
        int number = Random.Range(0, 4);
        numberCardTransform.GetComponent<Text>().text = (number + 1).ToString();
    }

    public void SetNumber(int number)
    {
        numberCardTransform.GetComponent<Text>().text = number.ToString();
        isClick = true;
    }

    public IEnumerator ScaleCard()
    {
        float timer = 0.4f;
        float lerpTime = 0;

        float startWidth = GetComponent<LayoutElement>().preferredWidth;
        float targetWidth = startWidth + ((startWidth * 50) / 100);

        float t = 0;

        while (lerpTime < timer)
        {
            t = lerpTime / timer;
            t = t * t * (3f - 2f * t);

            GetComponent<LayoutElement>().preferredWidth = Mathf.Lerp(startWidth, targetWidth, t);
            lerpTime += Time.deltaTime;
            yield return null;
        }

        GetComponent<LayoutElement>().preferredWidth = targetWidth;

        WaitForSeconds delay = new WaitForSeconds(1);
        yield return delay;

        StartCoroutine(RotateCard(LastAction));
    }

    void LastAction()
    {
        GetComponentInParent<PanelChooseOrder>().ShowRemainingCard(this);
    }

    public IEnumerator RotateCard(System.Action callback = null)
    {
        float timer = 0.25f;
        float lerpTime = 0;

        Vector3 startRotate = Vector3.zero;
        Vector3 targetRotate = new Vector3(0, 90, 0);

        while (lerpTime < timer)
        {
            frontCardTransform.eulerAngles = Vector3.Lerp(startRotate, targetRotate, lerpTime / timer);
            lerpTime += Time.deltaTime;
            yield return null;
        }

        frontCardTransform.eulerAngles = targetRotate;
        lerpTime = 0;
        startRotate = new Vector3(0, -90, 0);
        targetRotate = Vector3.zero;

        while (lerpTime < timer)
        {
            backCardTransform.eulerAngles = Vector3.Lerp(startRotate, targetRotate, lerpTime / timer);
            numberCardTransform.eulerAngles = Vector3.Lerp(startRotate, targetRotate, lerpTime / timer);
            lerpTime += Time.deltaTime;
            yield return null;
        }

        backCardTransform.eulerAngles = targetRotate;
        numberCardTransform.eulerAngles = targetRotate;

        WaitForSeconds delay = new WaitForSeconds(1);
        yield return delay;

        if (callback != null)
            callback();
    }
}