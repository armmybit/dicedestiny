﻿using UnityEngine;
using UI = UnityEngine.UI;

public class ItemShop : MonoBehaviour
{
    [Header("Basic")]
    public int id;

    [Header("UI")]
    public UI.Text textName;
    public UI.Text textDescription;
    public UI.Text textPrice;
    public UI.Button buttonBuy;
    //public UI.Image imageIcon;

    public void SetContent(int id, string name, string description, double price, bool isCanBuy)
    {
        this.id = id;

        this.textName.text = name;
        this.textDescription.text = description;
        this.textPrice.text = price + "z";
        this.buttonBuy.interactable = isCanBuy;
        //this.imageIcon.sprite = image;
    }
}