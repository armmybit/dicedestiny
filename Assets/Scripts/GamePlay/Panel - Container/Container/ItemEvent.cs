﻿using UnityEngine;
using UI = UnityEngine.UI;

public class ItemEvent : MonoBehaviour
{
    [Header("Basic")]
    public int id;

    [Header("UI")]
    public UI.Text textName;
    public UI.Text textDescription;
    public UI.Image imageIcon;

    public void SetContent(int shopId, string name, string description)
    {
        this.id = shopId;

        this.textName.text = name;
        this.textDescription.text = description;
        //imageIcon.sprite = image;
    }
}