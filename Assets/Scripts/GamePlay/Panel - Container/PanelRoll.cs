﻿using UnityEngine;

public class PanelRoll : Panel
{
    public void OnClick_Roll(int number)
    {
        GamePlayController.Instance.Roll((GameRoll)number);
    }
}