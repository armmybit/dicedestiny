﻿using System.Collections.Generic;
using UnityEngine;

public class PanelTarget : Panel
{
    public List<Target> targets = new List<Target>();

    public void SetTarget(Target target)
    {
        this.targets.Add(target);
    }

    public void Clear(GameObject container)
    {
        foreach (Transform child in container.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        this.targets.Clear();
    }
}