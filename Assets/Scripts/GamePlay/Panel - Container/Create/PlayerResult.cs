﻿using UnityEngine;
using UI = UnityEngine.UI;

public class PlayerResult : MonoBehaviour
{
    [Header("UI")]
    public UI.Text textRank;
    public UI.Text textName;
    public UI.Text textExp;

    public void Open(int rank, string name, double exp)
    {
        gameObject.SetActive(true);

        this.textRank.text = rank + "";
        this.textName.text = name;
        this.textExp.text = exp + "";
    }
}