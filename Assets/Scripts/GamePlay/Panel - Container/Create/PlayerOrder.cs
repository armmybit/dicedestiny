﻿using UnityEngine;
using UI = UnityEngine.UI;

public class PlayerOrder : MonoBehaviour
{
    [Header("UI")]
    public UI.Text textName;
    public UI.Text textOrder;

    public void SetContent(string name, int order)
    {
        this.textName.text = name;
        this.textOrder.text = order.ToString();
    }
}