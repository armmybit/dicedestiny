﻿using UnityEngine;
using UI = UnityEngine.UI;

public class Target : MonoBehaviour
{
    [Header("Data")]
    public int id;
    public CharacterType characterType;

    [Header("UI")]
    public UI.Text textName;

    public void SetContent(int id, string name)
    {
        this.id = id;
        this.textName.text = name;
    }

    public void OnClick_Target()
    {
        GamePlayController.Instance.Attack(CharacterType.Player, id);
    }
}