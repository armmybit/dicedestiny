﻿using System;
using UnityEngine;
using UI = UnityEngine.UI;

public class PlayerHud : MonoBehaviour
{
    [Header("Basic")]
    public Countdown countDown;

    [Header("UI")]
    public UI.Text textName;
    public UI.Text textLevel;
    public UI.Text textAtk;
    public UI.Text textHp;
    public UI.Text textCurrency;
    public UI.Text textDie;
    public UI.RawImage rawImageIcon;

    [Header("GameObject")]
    public GameObject imageEffect1;

    public void SetContent(PlayerProfile profile, PlayerStatusModel status, RenderTexture renderTexture)
    {
        SetName(profile.Name);
        SetLevel(profile.Level);
        SetAtk(status.Atk);
        SetHp(status.Hp);
        SetCurrency(status.Currency);
        SetIcon(renderTexture);
        ActiveDefeated(false);
    }

    public void StartCountDown(Action<bool> TurnCallback)
    {
        ActiveTurnUI(true);

        // Start Countdown
        this.countDown.Callback = TurnCallback;
        this.countDown.Begin();
    }

    public void ActiveTurnUI(bool isActive)
    {
        this.imageEffect1.SetActive(isActive);
        this.countDown.Clear();
    }

    public void SetName(string name)
    {
        this.textName.text = name;
    }

    public void SetLevel(int level)
    {
        this.textLevel.text = "Lv. " + level;
    }

    public void SetHp(double hp)
    {
        this.textHp.text = hp.ToString();

        if(hp <= 0)
            ActiveDefeated(true);
    }

    public void SetAtk(double atk)
    {
        this.textAtk.text = atk.ToString();
    }

    public void SetCurrency(double currency)
    {
        this.textCurrency.text = currency + "z";
    }

    private void SetIcon(RenderTexture renderTexture)
    {
        this.rawImageIcon.texture = renderTexture;
    }

    public void ActiveDefeated(bool isActive)
    {
        this.textDie.gameObject.SetActive(isActive);
    }
}
