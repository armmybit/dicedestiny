﻿using UnityEngine;
using UI = UnityEngine.UI;

public class PlayerOwnCard : MonoBehaviour
{
    [Header("Data")]
    public bool isActive;

    [Header("UI")]
    public UI.Text textTypeCard;
    public UI.Text textName;
    public UI.Text textDescription;
    public UI.Button button;

    public void SetContent(BuffInfoModel card)
    {
        this.isActive = card.TypeCard == CardType.Active;
        this.button.interactable = this.isActive;

        this.textTypeCard.text = card.TypeCard.ToString();
        this.textName.text = card.Name;
        this.textDescription.text = card.Effect;
    }

    public void OnClick_ActiveCard()
    {

    }

    public void Use()
    {

    }
}