﻿using UnityEngine;

public class Panel : MonoBehaviour
{
    public void Toggle(bool isActive)
    {
        gameObject.SetActive(isActive);
    }
}