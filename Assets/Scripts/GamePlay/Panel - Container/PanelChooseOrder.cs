﻿using System.Collections.Generic;
using System.Linq;

public class PanelChooseOrder : Panel
{
    public List<OrderCard> cards = new List<OrderCard>();
    public List<int> orderList = new List<int>();

    private OrderCard orderCard;

    void Awake()
    {
        this.cards = GetComponentsInChildren<OrderCard>().ToList();
    }

    public void ActiveCoin(int amount)
    {
        int i = 0;

        for (; i < amount; i++)
        {
            this.cards[i].gameObject.SetActive(true);
        }
    }

    private void InitCard()
    {
        for (int i = 0; i < this.cards.Count; i++)
        {
            var card = this.cards[i];
            card.SetNumber(i + 1);
        }
    }

    public void SetPlayerOrderList(List<int> order)
    {
        InitCard();

        int myIndex = order.IndexOf(User.Profile.Id);
        int myCardIndex = this.cards.IndexOf(orderCard);

        this.cards[myIndex].SetNumber(myCardIndex + 1);
        orderCard.SetNumber(myIndex + 1);

        StartCoroutine(this.orderCard.ScaleCard());
    }

    public void OnSelectCard(OrderCard card)
    {
        this.orderCard = card;
        GamePlayController.Instance.PlayerChooseOrder(0);
    }

    public void ShowRemainingCard(OrderCard card)
    {
        for (int i = 0; i < this.cards.Count; i++)
        {
            if (card != this.cards[i])
            {
                StartCoroutine(this.cards[i].GetComponent<OrderCard>().RotateCard());
            }
        }
    }
}