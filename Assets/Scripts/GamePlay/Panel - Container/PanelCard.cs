﻿using System.Collections.Generic;
using UnityEngine;

public class PanelCard : Panel
{
    public List<PlayerOwnCard> cardsObj = new List<PlayerOwnCard>();

    public void Setup(PlayerOwnCard cardObj)
    {
        this.cardsObj.Add(cardObj);
    }
}