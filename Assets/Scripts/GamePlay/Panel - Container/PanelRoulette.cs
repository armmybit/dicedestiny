﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class PanelRoulette : Panel
{
    List<int> playerIDs = new List<int> { 1, 2, 3, 4, 5, 6 };
    List<int> sectorsReward = new List<int>();
    public List<Sprite> characterIcons;

    public Transform container;

    public GameObject pieSample;
    public GameObject iconSample;

    private bool isStarted;
    public float[] sectorsAngles;
    private float finalAngle;
    private float startAngle = 0;
    private float currentLerpRotationTime;
    public Button TurnButton;
    public GameObject Circle;

    void Start()
    {
        float angle = 360f / playerIDs.Count;
        float fillPerCircle = 1f / playerIDs.Count;
        Vector3 center = transform.position;

        float angleOffset;
        if (playerIDs.Count % 2 == 0)
        {
            angleOffset = angle / 2;
        }
        else
        {
            angleOffset = angle;
        }


        for (int i = 0; i < playerIDs.Count; i++)
        {
            GameObject pie = Instantiate(pieSample, container, false);
            pie.SetActive(true);
            pie.GetComponent<Image>().fillAmount = fillPerCircle;
            pie.transform.eulerAngles = new Vector3(0, 0, (angle * i) + angleOffset);

            Vector3 pos = GetAngle(center, fillPerCircle * i, 2.0f);
            GameObject icon = Instantiate(iconSample, pos, Quaternion.identity, container);
            icon.SetActive(true);
            if (characterIcons.ElementAtOrDefault(i) != null)
            {

                icon.GetComponent<Image>().sprite = characterIcons[i];
            }
            else
            {
                icon.GetComponent<Image>().sprite = characterIcons[characterIcons.Count - 1];
            }

            icon.GetComponent<Image>().SetNativeSize();
            icon.transform.rotation = GetRotation(center, icon.transform.position);


            if (i == 0)
                sectorsReward.Add(0);
            else
                sectorsReward.Add(((int)angle * i) - 360);
        }


        sectorsAngles = new float[playerIDs.Count];

        for (int i = 0; i < playerIDs.Count; i++)
        {
            sectorsAngles[i] = angle * i;
        }
    }

    Vector3 GetAngle(Vector3 center, float fillPerCircle, float radius)
    {
        float ang = fillPerCircle * 360;
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = center.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.z = center.z;
        return pos;
    }

    Quaternion GetRotation(Vector3 center, Vector3 target)
    {
        Vector3 toTargetVector = center - target;
        float zRotation = (Mathf.Atan2(toTargetVector.y, toTargetVector.x) * Mathf.Rad2Deg) + 90.0f;
        return Quaternion.Euler(new Vector3(0, 0, zRotation));
    }


    public void SetRoulette(List<int> players)
    {
        playerIDs = players;
    }


    public void OnClick()
    {
        currentLerpRotationTime = 0f;

        int fullCircles = 10;
        float randomFinalAngle = sectorsAngles[UnityEngine.Random.Range(0, sectorsAngles.Length)];

        // Here we set up how many circles our wheel should rotate before stop
        finalAngle = -(fullCircles * 360 + randomFinalAngle);
        isStarted = true;
    }

    void Update()
    {
        // Make turn button non interactable if user has not enough money for the turn
        if (isStarted)// || CurrentCoinsAmount < TurnCost)
        {
            TurnButton.interactable = false;
            // TurnButton.GetComponent<Image>().color = new Color(255, 255, 255, 0.5f);
        }
        else
        {
            TurnButton.interactable = true;
            // TurnButton.GetComponent<Image>().color = new Color(255, 255, 255, 1);
        }

        if (!isStarted)
            return;

        float maxLerpRotationTime = 4f;

        // increment timer once per frame
        currentLerpRotationTime += Time.deltaTime;
        if (currentLerpRotationTime > maxLerpRotationTime || Circle.transform.eulerAngles.z == finalAngle)
        {
            currentLerpRotationTime = maxLerpRotationTime;
            isStarted = false;
            startAngle = finalAngle % 360;

            GiveAwardByAngle();
            // StartCoroutine(HideCoinsDelta());
        }

        // Calculate current position using linear interpolation
        float t = currentLerpRotationTime / maxLerpRotationTime;

        // This formulae allows to speed up at start and speed down at the end of rotation.
        // Try to change this values to customize the speed
        t = t * t * t * (t * (6f * t - 15f) + 10f);

        float angle = Mathf.Lerp(startAngle, finalAngle, t);
        Circle.transform.eulerAngles = new Vector3(0, 0, angle);
    }


    private void GiveAwardByAngle()
    {

        Debug.Log(">>>> " + sectorsReward.IndexOf((int)startAngle));

    }

}