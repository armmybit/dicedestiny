﻿using System.Collections;
using UnityEngine;

public class PanelEvent : Panel
{
    public Sprite[] sprieShop;
    public ItemEvent itemEvent;

    public void SetEvent(DetectBlockItemResponse events)
    {
        itemEvent.SetContent(events.Id, events.Name, events.Effect);

        StartCoroutine("CountToClose");
    }

    private IEnumerator CountToClose()
    {
        yield return new WaitForSeconds(1.5f);

        ReceivedEvent();
        Toggle(false);
    }

    private void ReceivedEvent()
    {
        GamePlayController.Instance.ReceiveEvent(itemEvent.id);
    }
}