﻿using System.Collections.Generic;
using UnityEngine;

public class PanelShop : Panel
{
    public Sprite[] sprieShop;
    public List<ItemShop> itemShops = new List<ItemShop>();

    public void SetShop(List<DetectBlockItemResponse> shops)
    {
        int i = 0;

        for (; i < shops.Count; i++)
        {
            var shop = shops[i];
            ItemShop itemShop = itemShops[i];
            itemShop.SetContent(shop.Id, shop.Name, shop.Effect, shop.Price, shop.IsCanBuy);
        }
    }

    public void OnClick_Cancel()
    {
        GamePlayController.Instance.BuyItem(false);
    }

    public void OnClick_Buy(ItemShop shop)
    {
        GamePlayController.Instance.BuyItem(true, shop.id);
    }
}