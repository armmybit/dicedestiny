﻿using UnityEngine;
using UI = UnityEngine.UI;

public class PanelTurn : Panel
{
    [Header("Data")]
    public CountdownToInactive countdown;

    [Header("Round")]
    public UI.Text textTurn;
    public UI.Text textPlayerTurn;

    public void SetContent(int round, string playerName)
    {
        this.textTurn.text = round.ToString();
        this.textPlayerTurn.text = playerName;

        this.countdown.Begin();
    }
}