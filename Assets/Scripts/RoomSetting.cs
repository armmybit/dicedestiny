﻿using System.Collections.Generic;

public static class RoomSetting
{
    public static int MapId { get; set; }
    public static int MaxPLayer { get; set; }
    public static string RoomId { get; set; }
    public static List<PlayerJoin> Players = new List<PlayerJoin>();
}