﻿using Newtonsoft.Json;
using UnityEngine;

public class TestSerialize : MonoBehaviour
{
    private void Start()
    {
        SerializeBasement();

        print("-----------------");

        SerializeCharacter();

        print("-----------------");

        SerializePlayer();

        print("-----------------");
    }

    private void SerializeBasement()
    {
        LoadderLocal.Load("Data/basements", (result) =>
        {
            BasementModel basementModel = JsonConvert.DeserializeObject<BasementModel>(result);

            print(JsonConvert.SerializeObject(basementModel));
        });
    }

    private void SerializeCharacter()
    {
        LoadderLocal.Load("Data/characters", (result) =>
        {
            CharacterModel characterModel = JsonConvert.DeserializeObject<CharacterModel>(result);

            print(JsonConvert.SerializeObject(characterModel));
        });
    }

    private void SerializePlayer()
    {
        LoadderLocal.Load("Data/player", (result) =>
        {
            PlayerModel playerModel = JsonConvert.DeserializeObject<PlayerModel>(result);

            print(JsonConvert.SerializeObject(playerModel));
        });
    }
}