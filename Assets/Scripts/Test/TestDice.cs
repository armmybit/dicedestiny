﻿using System.Collections;
using Newtonsoft.Json;
using UnityEngine;

public class TestDice : MonoBehaviour
{
    public Dice dice1;
    public Dice dice2;

    public int number1;
    public int number2;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.5f);

        this.dice1.Play(number1);
        this.dice2.Play(number2);
    }
}