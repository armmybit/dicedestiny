﻿using System.Collections;
using System.Collections.Generic;

public interface IGame
{
    // Player connect to Gameplay
    void PlayerConnectedToGamePlay();
    void OnPlayerConnectedToGamePlay(PlayerConnectedResponse res);

    // Players ready
    void OnCompletedPlayersConnected(ConnectedPlayersResponse res);

    // Choose Order
    void PlayerChooseOrder(int index);
    void OnPlayerChooseOrder(ChooseOrderResponse res);
    IEnumerator DisplayOrder(ChooseOrderResponse res);

    void PrepareGame(PrepareGameResponse res);

    void CreatePath(List<Block> blocksInfo);
    void CreatePlayer(List<int> orders);
    void CreateMonster(Monster monster, Block blockUpdate);

    // Get and Use card by turn
    void PlayerDrawCard(PlayerDrawCard card);
    void UseCard(int cardId, int playerTarget);
    void OnUseCard();

    // Game Start
    void StartGame(StartGameResponse res);

    // Player Turn
    void PlayerTurn(int id, int round, Dictionary<BuffType, List<PlayerBuffOnGoing>> buffs = null);

    // Roll Dice
    void Roll(GameRoll roll);
    void OnRoll(RollResponse res);

    // When player stop at block will detect type of block and opponent
    void PlayerArrived(Block info, BlockCheckState state = BlockCheckState.DetectOpponent);
    void OnDetectOpponent(DetectOpponentResponse res);
    void OnDetectBlock(DetectBlockResponse res);

    // Battle
    void BattlePlayer(ModelPlayer opponent, BlockCheckState state = BlockCheckState.End);
    void BattlePlayer(ModelMonster opponent, BlockCheckState state = BlockCheckState.End);
    void BattleMonster(int opponent);
    void OnBattleEnd(BlockCheckState state);

    // Attack player and monster
    void Attack(CharacterType characterType, int id);
    void OnPlayerAttack(AttackResponse res);

    // Purchase Item from shop
    void BuyItem(bool isBuy, int shopId = -1);
    void OnBuyItem(BuyItemResponse res);

    // Get Event Buff
    void ReceiveEvent(int eventId);
    void OnReceiveEvent(ReceiveEventResponse res);

    // End Turn
    void EndTurn();
    void OnEndTurn(EndTurnResponse res);

    // End Game
    void OnEndGame(EndGameResponse res);
}