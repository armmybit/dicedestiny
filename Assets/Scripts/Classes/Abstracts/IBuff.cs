﻿public abstract class IBuff
{
    public BuffInfoModel buff;
    public ModelPlayer self;
    public BuffState state;

    public virtual void Apply(BuffInfoModel b, ModelPlayer p, BuffState s)
    {
        buff = (BuffInfoModel)b.Clone();
        self = p;
        state = s;
    }

    public virtual void Update(BuffState updateState)
    {
        state = updateState;
    }

    public virtual void Restore(BuffState restoreState)
    {
        state = restoreState;
    }

    public virtual void Use(ShopEffectStateModel effect) { }
    public virtual void Use(EventEffectStateModel effect) { }
}