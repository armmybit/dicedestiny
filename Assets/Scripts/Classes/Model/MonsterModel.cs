﻿using System.Collections.Generic;

public class MonsterModel
{
    public List<MonsterTypeInfoModel> Data = new List<MonsterTypeInfoModel>();
}

public class MonsterTypeInfoModel
{
    public MonsterType MonsterType { get; set; }
    public List<MonsterProfileModel> Collections = new List<MonsterProfileModel>();
}

public class MonsterProfileModel
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Level { get; set; }
    public MonsterStatusModel Status { get; set; }
}

public class MonsterStatusModel
{
    public double Hp { get; set; }
    public double Atk { get; set; }
    public MonsterAbility Status { get; set; }
}