﻿using System;
using System.Collections.Generic;

public class BuffModel
{
    public List<BuffInfoModel> Data = new List<BuffInfoModel>();
}

public class BuffInfoModel : ICloneable
{
    // For all Type
    public int Id { get; set; }
    public string Name { get; set; }
    public string Effect { get; set; }
    public int RemainUse { get; set; }
    public int RemainTurn { get; set; }
    public bool IsEffectiveEveryTurn { get; set; }
    public bool IsReset { get; set; }
    public BuffType TypeBuff { get; set; }
    public Buff Type { get; set; }
    public bool IsImmediateDestroy { get; set; }

    // For Card
    public CardType TypeCard { get; set; }
    public int Number { get; set; }

    // For Shop
    public double Price { get; set; }

    public object Clone()
    {
        return this.MemberwiseClone();
    }
}