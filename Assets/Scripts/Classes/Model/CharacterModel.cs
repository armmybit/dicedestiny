﻿using System.Collections.Generic;

public class CharacterModel
{
    public List<Character> Characters = new List<Character>();
}

public class Character
{
    public int Id { get; set; }
    public string Name { get; set; }
    public List<CharacterCollection> Collections = new List<CharacterCollection>();
}

public class CharacterCollection
{
    public int Id { get; set; }
    public string Name { get; set; }
}