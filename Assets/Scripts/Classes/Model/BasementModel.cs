﻿using System.Collections.Generic;

public class BasementModel
{
    public List<Basement> Basements = new List<Basement>();
}

public class Basement
{
    public int Id { get; set; }
    public string Name { get; set; }
}
