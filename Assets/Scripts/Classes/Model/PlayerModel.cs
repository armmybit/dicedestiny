﻿using System.Collections.Generic;

public class PlayerModel
{
    public PlayerInfoModel Data { get; set; }
}

public class PlayerInfoModel
{
    public PlayerProfileModel Profile { get; set; }
    public PlayerCurrentFormModel CurrentInGame { get; set; }
    public List<PlayerFormCharacterModel> Characters = new List<PlayerFormCharacterModel>();
    public List<PlayerFormBasementModel> Basements = new List<PlayerFormBasementModel>();
}

public class PlayerProfileModel
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Level { get; set; }
}

public class PlayerCurrentFormModel
{
    public PlayerFormCharacterModel Main { get; set; }
    public PlayerFormCharacterModel Sub1 { get; set; }
    public PlayerFormCharacterModel Sub2 { get; set; }
    public PlayerFormBasementModel Basement { get; set; }
}

public class PlayerFormCharacterModel
{
    public int ClassId { get; set; }
    public int CharacterId { get; set; }
}

public class PlayerFormBasementModel
{
    public int Id { get; set; }
}