﻿using UnityEngine;

public class Dice : MonoBehaviour
{
    public float force = 1;
    public Vector3 dragLastAngle;
    public Vector3 directionForce;
    public Vector3 defaultPosition;

    private void Start()
    {
        defaultPosition = transform.localPosition;
    }

    public void Play(int number)
    {
        SetDiceFace(number);
        AddProperties();
    }

    public void AddProperties()
    {
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().AddForce(directionForce * force, ForceMode.VelocityChange);
        GetComponent<Rigidbody>().AddTorque(dragLastAngle.normalized * force, ForceMode.Impulse);
    }

    private void SetDiceFace(int number)
    {
        switch (number)
        {
            case 1:
                gameObject.transform.rotation = Quaternion.Euler(0, 90, 0);
                break;

            case 2:
                gameObject.transform.rotation = Quaternion.Euler(0, 0, 90);
                break;

            case 3:
                gameObject.transform.rotation = Quaternion.Euler(0, 180, 0);
                break;

            case 4:
                gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
                break;

            case 5:
                gameObject.transform.rotation = Quaternion.Euler(0, 0, 270);
                break;

            case 6:
                gameObject.transform.rotation = Quaternion.Euler(0, 270, 0);
                break;
        }
    }

    public void Clear()
    {
        transform.localPosition = defaultPosition;
    }
}

public class DiceFace
{
    public int Number1 { get; set; }
    public int Number2 { get; set; }
}