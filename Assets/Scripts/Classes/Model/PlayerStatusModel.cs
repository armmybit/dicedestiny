﻿public class PlayerStatusModel
{
    public bool IsBot { get; set; }
    public double MaxHp { get; set; }
    public double Hp { get; set; }
    public double Atk { get; set; }
    public double Def { get; set; }
    public double Currency { get; set; }
}