﻿
public class PlayerJoinModel
{
    public int Id { get; set; }
    public PlayerFormCharacterModel Main { get; set; }
    public PlayerFormCharacterModel Sub1 { get; set; }
    public PlayerFormCharacterModel Sub2 { get; set; }
    public PlayerFormBasementModel Basement { get; set; }
}