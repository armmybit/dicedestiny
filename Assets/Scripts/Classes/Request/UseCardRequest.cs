﻿
public class UseCardRequest : CommonRequest
{
    public int PlayerId { get; set; }
    public int CardId { get; set; }
    public int TargetId { get; set; }
}