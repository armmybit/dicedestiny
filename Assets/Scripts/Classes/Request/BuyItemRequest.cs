﻿
public class BuyItemRequest : BasicRequest
{
    public bool IsBuy { get; set; }
    public int ShopId { get; set; }
}