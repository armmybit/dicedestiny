﻿
public class CreateRoomRequest
{
    public int MapId { get; set; }
    public int MaxPlayer { get; set; }
    public PlayerJoinModel Player { get; set; }
}