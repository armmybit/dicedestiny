﻿
public class PlayerConnectedRequest : CommonRequest
{
    public int PlayerId { get; set; }
}