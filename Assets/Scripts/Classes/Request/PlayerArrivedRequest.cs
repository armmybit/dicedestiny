﻿
public class PlayerArrivedRequest : BasicRequest
{
    public BlockCheckState State { get; set; }
    public Block Info { get; set; }
}