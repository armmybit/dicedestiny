﻿using System.Collections.Generic;

public class ChooseOrderResponse
{
    public GameState GameState { get; set; }
    public List<int> Numbers = new List<int>();
}