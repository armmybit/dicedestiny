﻿
using System.Collections.Generic;

public class EndGameResponse
{
    public int Winner { get; set; }
    public List<int> Loosers = new List<int>();
}