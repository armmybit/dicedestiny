﻿
public class StartGameResponse
{
    public GameState GameState { get; set; }
    public int Id { get; set; }
    public int Round { get; set; }
    public bool IsCardAvailable { get; set; }
}