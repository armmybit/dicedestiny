﻿public class BuyItemResponse
{
    public bool IsCancel { get; set; }
    public double Currency { get; set; }
    public BuffInfoModel Buff { get; set; }
    public BuffState State { get; set; }
    public ShopEffectStateModel Effect { get; set; }
}