﻿using System.Collections.Generic;

public class PrepareGameResponse
{
    public GameState GameState { get; set; }
    public List<Block> Blocks = new List<Block>();
    public List<int> Orders = new List<int>();
}