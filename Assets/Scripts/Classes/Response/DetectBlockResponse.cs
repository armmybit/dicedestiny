﻿
using System.Collections.Generic;

public class DetectBlockResponse
{
    public BlockType BlockType { get; set; }

    // Castle
    public int BaseLevel { get; set; }
    public double Currency { get; set; }

    // Event
    public DetectBlockItemResponse EventItem { get; set; }

    // Shop
    public List<DetectBlockItemResponse> ShopsItem = new List<DetectBlockItemResponse>();

    // Flag
    public List<int> Targets = new List<int>();

    // Treasure

    // Monster
    public Block BlockUpdate { get; set; }
    public bool IsCreateNew { get; set; }
    public Monster Monster { get; set; }
}

public class DetectBlockItemResponse
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Effect { get; set; }
    public double Price { get; set; }
    public bool IsCanBuy { get; set; }
}