﻿using System.Collections.Generic;

public class ReceiveEventResponse
{
    public BuffInfoModel Buff { get; set; }
    public BuffState State { get; set; }
    public EventEffectStateModel Effect { get; set; }
}