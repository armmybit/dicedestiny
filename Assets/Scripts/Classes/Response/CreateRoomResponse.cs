﻿
using System.Collections.Generic;

public class CreateRoomResponse
{
    public string RoomId { get; set; }
    public List<PlayerJoin> Players { get; set; }
}

public class PlayerJoin
{
    public PlayerProfileModel Profile { get; set; }
    public PlayerStatusModel Status { get; set; }
    public PlayerFormBasementModel Basement { get; set; }
    public PlayerFormCharacterModel Main { get; set; }
    public PlayerFormCharacterModel Sub1 { get; set; }
    public PlayerFormCharacterModel Sub2 { get; set; }
}