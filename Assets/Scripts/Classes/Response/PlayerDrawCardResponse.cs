﻿using System.Collections.Generic;

public class PlayerDrawCardResponse
{
    public GameState GameState { get; set; }
    public List<PlayerDrawCard> Players = new List<PlayerDrawCard>();
}