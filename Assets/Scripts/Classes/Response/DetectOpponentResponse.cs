﻿using System.Collections.Generic;

public class DetectOpponentResponse
{
    public ServerAttackResponse Opponent { get; set; }
    public Block BlockUpdate { get; set; }
}