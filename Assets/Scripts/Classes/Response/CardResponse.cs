﻿
public class CardResponse
{
    public int CardId { get; set; }
    public string Name { get; set; }
    public string Effect { get; set; }
    public BuffType TypeBuff { get; set; }
    public Buff Type { get; set; }
}