﻿using System.Collections.Generic;

public class EndTurnResponse
{
    public int Id { get; set; }
    public int Round { get; set; }
    public PlayerDrawCard Card { get; set; }
    public Dictionary<BuffType, List<PlayerBuffOnGoing>> Buffs = new Dictionary<BuffType, List<PlayerBuffOnGoing>>();
}

public class PlayerDrawCard
{
    public int CardId { get; set; }
    public string Name { get; set; }
    public string Effect { get; set; }
    public Buff Type { get; set; }
    public CardType TypeCard { get; set; }
}

public class PlayerBuffOnGoing
{
    public int Id { get; set; }
    public int RemainTurn { get; set; }
    public BuffState State { get; set; }
}