﻿public enum GameCamera
{
    Follow,
    Overview,
    Battle,
    BattleFocus
}