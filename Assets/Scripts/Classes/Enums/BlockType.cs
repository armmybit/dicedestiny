﻿public enum BlockType
{
    None = 0,
    Castle = 1,
    Flag = 2,
    Monster = 3,
    Event = 4,
    Shop = 5,
    Treasure = 6
}