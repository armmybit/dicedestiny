﻿public enum BuffType
{
    Card,
    Shop,
    Event,
    Skill
}

public enum Buff
{
    Buff,
    Debuff,
    Common
}

public enum BuffState
{
    NotReady,
    Ready,
    Using,
    Expired
}

// Case Card
public enum CardType
{
    None = -1,
    Active = 0,
    Trigger = 1
}

public enum CardName
{
    Dodge = 0,
    Protect = 1,
    Attack = 2,
    Hide = 3,
    Cure = 4,
    Slide = 5,
    KnockBack = 6,
    Root = 7,
    Sleep = 8,
    Poision = 9
}

// Case Shop
public enum ShopName
{
    TheDice = 0,
    RedPotion = 1,
    Wing = 2,
    TomeOfKnowledge = 3,
    GoddessTear = 4,
    AttackPotion = 5,
    DefPotion = 6
}

// Case Event
public enum EventName
{
    AmorBreak = 0,
    WeaponBreak = 1,
    Damage = 2,
    MeteorStrike = 3,
    StormGust = 4,
    MonsterInvades = 5,
    Heal = 6,
    DamageUp = 7,
    DefenseUp = 8,
    ReStart_Teleport = 9,
    Reverse = 10,
    Slow = 11,
    SpeedBoost = 12
}