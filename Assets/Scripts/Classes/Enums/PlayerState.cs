﻿
public enum PlayerState
{
    None,
    Start,
    ActiveCard,
    Roll,
    Event,
    Battle,
    End
}