﻿public enum MonsterType
{
    LowTier,
    MiddleTier,
    TopTier,
    Mvp
}

public enum MonsterAbility
{
    None,
    Take1Damage
}