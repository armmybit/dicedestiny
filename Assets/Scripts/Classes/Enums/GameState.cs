﻿
public enum GameState
{
    None,
    Prepare,
    ChooseOrder,
    ShowOrder,
    DrawCard,
    Start,
    End
}