﻿using System;
using System.Collections;
using UnityEngine;
using UI = UnityEngine.UI;

public class Countdown : MonoBehaviour
{
    [Header("Basic")]
    public float time;

    public bool isCount = false;
    public Action<bool> Callback;

    [Header("UI")]
    public UI.Image timebarImage;

    public void Begin()
    {
        Clear();
        isCount = true;
    }

    public void Pause()
    {
        isCount = false;
    }

    public void Clear()
    {
        isCount = false;
        timebarImage.fillAmount = 1;
        Callback = null;
    }

    private IEnumerator Count()
    {
        float normalizedTime = 0;

        while (normalizedTime <= 1f)
        {
            timebarImage.fillAmount = normalizedTime;
            normalizedTime += Time.deltaTime / time;
            yield return null;
        }

        if (normalizedTime > 1)
            SendBack(true);
    }

    private void Update()
    {
        if (isCount)
        {
            timebarImage.fillAmount -= 1.0f / time * Time.deltaTime;

            if (timebarImage.fillAmount <= 0)
            {
                SendBack(true);
            }
        }
    }

    public void SendBack(bool isTimeout)
    {
        Callback?.Invoke(isTimeout);
    }
}