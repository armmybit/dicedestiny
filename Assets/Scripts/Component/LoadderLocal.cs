﻿using System;
using UnityEngine;

public class LoadderLocal
{
    public static void Load(string url, Action<string> callback)
    {
        TextAsset textAsset = Resources.Load<TextAsset>(url);
        string data = textAsset.text;
        callback(data);
    }

    public static void Load(string url, Action<Texture> callback)
    {
        Texture texture = Resources.Load<Texture>(url);
        callback(texture);
    }
}