﻿using UnityEngine;

public class Scaller : MonoBehaviour
{
    public GameObject self;
    public float duration;
    public bool isStart;

    public void Play()
    {
        this.isStart = true;
    }

    private void FixedUpdate()
    {
        if (!this.isStart)
            return;

        if (this.duration > 0)
        {
            self.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, Time.deltaTime);
        }
        else
        {
            self.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, Time.deltaTime);
        }
    }
}