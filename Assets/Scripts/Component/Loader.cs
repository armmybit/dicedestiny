﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class Loader : MonoBehaviour
{
    public enum RequestTypeEnum
    {
        Local,
        Http
    }

    public void Load(RequestTypeEnum loadderActionEnum, string url, Action<Texture> callback)
    {
        StartCoroutine(StartLoadTexture(loadderActionEnum, url, callback));
    }

    public void Load(RequestTypeEnum loadderActionEnum, string url, Action<string> callback)
    {
        StartCoroutine(StartLoadFile(loadderActionEnum, url, callback));
    }

    private IEnumerator StartLoadTexture(RequestTypeEnum loadderActionEnum, string url, Action<Texture> callback)
    {
        switch (loadderActionEnum)
        {
            case RequestTypeEnum.Local:

                Texture texture = Resources.Load<Texture>(url);

                if (callback != null)
                    callback(texture);

                break;

            case RequestTypeEnum.Http:

                UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);

                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {
                    callback(null);
                }
                else
                {
                    Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;

                    callback(myTexture);
                }

                break;
        }
    }

    private IEnumerator StartLoadFile(RequestTypeEnum loadderActionEnum, string url, Action<string> callback)
    {
        switch (loadderActionEnum)
        {
            case RequestTypeEnum.Local:

                TextAsset data = Resources.Load<TextAsset>(url);

                callback(data.text);

                break;

            case RequestTypeEnum.Http:

                UnityWebRequest www = UnityWebRequest.Get(url);

                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {
                    callback(null);
                }
                else
                {
                    callback(www.downloadHandler.text);
                }

                break;
        }
    }
}