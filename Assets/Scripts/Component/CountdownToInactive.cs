﻿using UnityEngine;

public class CountdownToInactive : MonoBehaviour
{
    [Header("Basic")]
    public float define = 3f;
    public float time;
    public bool isCount = false;

    public void Begin()
    {
        Clear();
        this.isCount = true;
    }

    private void Clear()
    {
        gameObject.SetActive(true);
        this.isCount = false;
        this.time = define;
    }

    private void Update()
    {
        if (this.isCount)
        {
            this.time -= Time.deltaTime;

            if (this.time <= 0)
            {
                gameObject.SetActive(false);
            }
        }
    }
}