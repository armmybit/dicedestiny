﻿using System;
using UnityEngine;

public class DestroyTime : MonoBehaviour
{
    [Header("Basic")]
    public float time;
    public bool isCount = false;
    public Action Callback;

    public void OnEnable()
    {
        isCount = true;
    }

    public void OnDisable()
    {
        isCount = false;
        Callback = null;
    }

    private void Update()
    {
        if (isCount)
        {
            time -= Time.deltaTime;

            if (time < 0)
            {
                SendBack();
            }
        }
    }

    public void SendBack()
    {
        Callback?.Invoke();
    }
}