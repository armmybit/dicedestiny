﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class Util
{
    public static string ConvertDateTimeToStr(DateTime dateTime)
    {
        return dateTime.ToString("yyyyMMdd");
    }

    public static GameObject AddChild(GameObject parent, GameObject prefab, int index = 0)
    {
        GameObject go = GameObject.Instantiate(prefab);

        if (go != null && parent != null)
        {
            Transform t = go.transform;
            t.SetParent(parent.transform);
            t.localPosition = prefab.transform.localPosition;
            t.localRotation = prefab.transform.localRotation;
            t.localScale = prefab.transform.localScale;
            go.layer = parent.layer;
            go.name = "" + index;
        }

        return go;
    }

    public static void MoveToRealParent(GameObject parent, GameObject go)
    {
        if (go != null && parent != null)
        {
            Transform t = go.transform;
            t.SetParent(parent.transform);
            t.localPosition = Vector3.zero;
            t.localScale = Vector3.one;
            go.layer = parent.layer;
        }
    }

    public static void SetLayerRecursively(GameObject go, int layerNumber)
    {
        foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
        {
            trans.gameObject.layer = layerNumber;
        }
    }


    // Note that Color32 and Color implictly convert to each other. You may pass a Color object to this method without first casting it.
    public static string ColorToHex(Color32 color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }

    public static Color HexToColor(string hex)
    {
        byte r = byte.Parse(hex.Substring(1, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(3, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(5, 2), System.Globalization.NumberStyles.HexNumber);

        return new Color32(r, g, b, 255);
    }

    public static string[] DateTH = new string[] { "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤศภาคม", "มิถุนายน", "กรกฏาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พศจิกายน", "ธันวาคม" };

    public static int GetColorIndex(string color)
    {
        switch (color.ToUpper())
        {
            case "#FF0000":
                return 0;

            case "#0000FF":
                return 1;

            case "#00FF00":
                return 2;

            case "#FFFF00":
                return 3;
        }

        return 0;
    }

    public static string GetTitleColor(string color)
    {
        switch (color.ToUpper())
        {
            case "#FF0000":
                return "RED BINGO";

            case "#0000FF":
                return "BLUE BINGO";

            case "#00FF00":
                return "GREEN BINGO";

            case "#FFFF00":
                return "YELLOW BINGO";
        }

        return "RED BINGO";
    }

    public static DateTime GetTime(DateTime currentTime)
    {
#if UNITY_EDITOR
        return currentTime;
#else
        return currentTime.AddHours(7).AddMinutes(0);
#endif
    }

    #region MD5

    private static readonly string PasswordHash = "P@@Sw0rd";
    private static readonly string SaltKey = "S@LT&KEY";
    private static readonly string VIKey = "@1B2c3D4e5F6g7H8";

    public static string Encrypt(object plainText)
    {
        byte[] plainTextBytes = Encoding.UTF8.GetBytes((string)plainText);

        byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
        var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
        var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

        byte[] cipherTextBytes;

        using (var memoryStream = new MemoryStream())
        {
            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
            {
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                cryptoStream.FlushFinalBlock();
                cipherTextBytes = memoryStream.ToArray();
                cryptoStream.Close();
            }
            memoryStream.Close();
        }

        return Convert.ToBase64String(cipherTextBytes);
    }

    public static string Decrypt(string encryptedText)
    {
        byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
        byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
        var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

        var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
        var memoryStream = new MemoryStream(cipherTextBytes);
        var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
        byte[] plainTextBytes = new byte[cipherTextBytes.Length];

        int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
        memoryStream.Close();
        cryptoStream.Close();
        return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
    }

    #endregion MD5
}

public struct JsonDateTime
{
    public long value;

    public static implicit operator DateTime(JsonDateTime jdt)
    {
        Debug.Log("Converted to time");

        return DateTime.FromFileTime(jdt.value);
    }

    public static implicit operator JsonDateTime(DateTime dt)
    {
        Debug.Log("Convert to JDT");
        JsonDateTime jdt = new JsonDateTime
        {
            value = dt.ToFileTimeUtc()
        };

        return jdt;
    }
}