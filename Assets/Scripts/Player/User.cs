﻿using System.Collections.Generic;

public static class User
{
    public static PlayerProfileModel Profile { get; set; }
    public static PlayerCurrentFormModel CurrentIngame { get; set; }
    public static List<PlayerFormCharacterModel> Characters = new List<PlayerFormCharacterModel>();
    public static List<PlayerFormBasementModel> Basements = new List<PlayerFormBasementModel>();

    public static void Set(PlayerModel model)
    {
        Profile = model.Data.Profile;
        CurrentIngame = model.Data.CurrentInGame;
        Characters = model.Data.Characters;
        Basements = model.Data.Basements;
    }
}